﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class ShortPatientDTO
    {
        public int Id { get; set; }
        public string PatientNumber { get; set; }
        public DateTime DateOfBirthday { get; set; }
        public DateTime LastVisitDate { get; set; }
        public List<MedicineNameDTO> DispensedMedicines { get; set; }
    }
}
