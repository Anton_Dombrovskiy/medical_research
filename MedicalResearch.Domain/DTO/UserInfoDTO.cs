﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class UserInfoDTO
    {
        public int Id { get; set; }
        public string Email { get; private set; }
        public bool IsLocked { get; set; }
        public Role Role { get; set; }
        public string AttachedClinic { get; set; }
    }
}
