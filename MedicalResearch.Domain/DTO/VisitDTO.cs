﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class VisitDTO
    {
        public int Id { get; set; }
        public DateTime DateOfVisit { get; set; }
        public string DispencedMedicine { get; set; }
    }
}
