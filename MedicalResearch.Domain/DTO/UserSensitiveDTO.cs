﻿using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.DTO
{
    public class UserSensitiveDTO
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string AttachedClinic { get; set; }
        public bool IsLocked { get; set; }
    }
}