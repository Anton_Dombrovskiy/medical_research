﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class ClinicNameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
