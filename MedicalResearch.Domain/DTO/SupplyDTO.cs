﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class SupplyDTO
    {
        public int Id { get; set; }
        public string ClinicName { get; set; }
        public string MedicineName { get; set; }
        public int Amount { get; set; }
    }
}
