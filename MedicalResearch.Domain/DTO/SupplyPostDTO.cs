﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class SupplyPostDTO
    {
        public int Id { get; set; }
        public int ClinicId{ get; set; }
        public int MedicineId { get; set; }
        public int Amount { get; set; }
    }
}
