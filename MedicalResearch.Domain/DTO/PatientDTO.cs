﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class PatientDTO
    {
        public int Id { get; set; }
        //format: {Clinic number}-{4 numbers}
        public string PatientNumber { get; set; }
        public DateTime DateOfBirthday { get; set; }
        public Sex Sex { get; set; }
        public PatientStatus Status { get; set; }
        public char MedicineType { get; set; }
        public DateTime LastVisitDate { get; set; }
    }
}
