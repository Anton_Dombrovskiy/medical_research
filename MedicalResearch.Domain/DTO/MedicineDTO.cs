﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class MedicineDTO
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public char Type { get; set; }
        public string DosageForm { get; set; }
        public string Container { get; set; }
        public MedicineState State { get; set; }
        public DateTime ExpireAt { get; set; }
    }
}
