﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.DTO
{
    public class DetailedResearchProjectDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateOfNews { get; set; }
        public string BackgroundImageName { get; set; }
        public string DetailedInformation { get; set; }
    }
}
