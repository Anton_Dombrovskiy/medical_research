﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    public class AddPatientDTO
    {
        public int Id { get; set; }
        public string PatientNumber { get; set; }
        public DateTime DateOfBirthday { get; set; }
        public Sex Sex { get; set; }
    }
}
