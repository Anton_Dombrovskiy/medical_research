﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents the clinic
    /// </summary>
    public class Clinic
    {
        //Should be three digits long
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string PrimaryAddress { get; set; }
        public string SecondaryAddress { get; set; }
        public string PhoneNumber { get; set; }
    }
}
