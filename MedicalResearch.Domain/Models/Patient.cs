﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents the patient
    /// </summary>
    public class Patient
    {
        public int Id { get; set; }
        //format: {Clinic number}-{4 numbers}
        public string PatientNumber { get; set; }
        public DateTime DateOfBirthday { get; set; }
        public Sex Sex { get; set; }
        public PatientStatus Status { get; set; }
        public char MedicineType { get; set; }
        public Clinic Clinic { get; set; }
        public DateTime LastVisitDate { get; set; }
    }

    public enum PatientStatus
    {
        Screened,
        Randomized,
        Finished,
        FinishedEarly
    }

    public enum Sex
    {
        Male,
        Female,
        Other
    }
}
