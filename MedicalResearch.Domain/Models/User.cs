﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents the user account
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Initials { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserAvatarName { get; set; }
        public bool IsLocked { get; set; }
        public Role Role { get; set; }
        public string AttachedClinic { get; set; }
    }
}
