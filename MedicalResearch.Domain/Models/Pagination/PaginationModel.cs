﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models.Pagination
{
    public class PaginationModel
    {
        public int PageNumber { get; private set; }
        public int TotalPages { get; private set; }
        private int _totalAmountOfPages;
        private int _pageSize;

        public PaginationModel(int count, int pageNumber, int pageSize)
        {
            _totalAmountOfPages = count;
            _pageSize = pageSize;
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage
        {
            get
            {
                return PageNumber > 1;
            }
        }

        public bool HasNextPage
        {
            get
            {
                return PageNumber < TotalPages;
            }
        }

        public object GetPaginationMetadata()
        {
            return new
            {
                TotalAmountOfRecords = _totalAmountOfPages,
                TotalPages = TotalPages,
                HasNextPage = HasNextPage,
                HasPreviousPage = HasPreviousPage,
                CurrentPage = PageNumber,
                PageSize = _pageSize
            };
        }
    }
}
