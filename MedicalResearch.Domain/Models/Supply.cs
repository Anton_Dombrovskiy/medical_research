﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents the supply
    /// </summary>
    public class Supply
    {
        public int Id { get; set; }
        public Clinic Clinic { get; set; }
        public Medicine Medicine { get; set; }
        public int Amount { get; set; }
        public bool IsSupplied { get; set; } = false;
    }
}
