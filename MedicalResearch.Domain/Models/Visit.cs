﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents the patient visit
    /// </summary>
    public class Visit
    {
        public int Id { get; set; }
        public DateTime DateOfVisit { get; set; }
        public Medicine DispencedMedicine { get; set; }
        public Patient Patient { get; set; }
    }
}
