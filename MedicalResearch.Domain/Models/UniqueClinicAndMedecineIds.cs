﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    public class UniqueClinicAndMedecineIds
    {
        public int Id { get; set; }
        public int MedicineUniqueId { get; set; }
        public int ClinicUniqueId { get; set; }
    }
}
