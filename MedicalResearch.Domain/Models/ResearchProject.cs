﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents information about medical research
    /// </summary>
    public class ResearchProject
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime DateOfNews { get; set; }
        public string BackgroundImageName { get; set; }
        public string DetailedInformation { get; set; }
    }
}
