﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedicalResearch.Domain.Models
{
    /// <summary>
    /// The class represents the medicine
    /// </summary>
    public class Medicine
    {
        //Should be six digits long
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string Description { get; set; }
        public char Type { get; set; }
        public string DosageForm { get; set; }
        public string Container { get; set; }
        public MedicineState State { get; set; }
        public DateTime ExpireAt { get; set; }
    }

    public enum MedicineState
    {
        Ok,
        Damaged,
        Lost
    }
}
