﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Services
{
    public interface IClinicService
    {
        Task AddClinicAsync(Clinic clinic);
    }
}
