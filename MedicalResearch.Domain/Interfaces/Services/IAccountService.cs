﻿using System.Threading.Tasks;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Http;

namespace MedicalResearch.Domain.Interfaces.Services
{
    public interface IAccountService
    {
        Task UpdateAccountInformationAsync(User user, UpdateUserDTO updatedUser);
        Task UpdateSensitiveInformationAsync(User user, UserSensitiveDTO updatedUser);
        Task AddUserAsync(User user);
        Task RemoveUserAsync(int id);
        Task<string> UploadPhotoAsync(IFormFile uploadedFile);
    }
}