﻿using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Services
{
    public interface IProjectService
    {
        Task<string> UploadPhotoAsync(IFormFile uploadedFile);
        Task AddProjectAsync(ResearchProject project);
        Task UpdateProjectAsync(ResearchProject project, DetailedResearchProjectDTO updatedProject);
    }
}
