﻿using MedicalResearch.Domain.Models.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(ContactEmail email);
    }
}
