﻿using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Services
{
    public interface IPatientService
    {
        Task<Patient> AddPatientAsync(AddPatientDTO patient);
        Task PerformVisitAsync(int patientId);
        Task EndParticipationAsync(int patientId);
        Task<IEnumerable<MedicineNameDTO>> GetAllPatientDispensedMedicinesAsync(int patientId);
        Task<IEnumerable<Patient>> GetAllPatientAsync(bool specifyClinic, string clinicId = null);
    }
}
