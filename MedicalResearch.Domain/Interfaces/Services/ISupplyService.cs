﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Interfaces.Services
{
    public interface ISupplyService
    {
        Task<Supply> AddSupplyAsync(SupplyPostDTO supply);
        Task SupplyMedicineAsync();
        Task DeleteSupply(int id);
    }
}
