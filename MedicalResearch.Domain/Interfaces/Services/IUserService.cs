﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Services
{
    interface IUserService
    {
        void RegisterUser(); //TODO: create model with fields to register like RegisterModel
        void LoginUser();   //TODO: create model with fields to login like LoginModel
        void RemoveAccount();
    }
}
