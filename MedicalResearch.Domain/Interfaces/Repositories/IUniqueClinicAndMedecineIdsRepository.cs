﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IUniqueClinicAndMedecineIdsRepository<T> : IRepository<T>
        where T : class
    {
        Task<int> GetUniqueMedicineIdAsync();
        Task<int> GetUniqueClinicIdAsync();
    }
}
