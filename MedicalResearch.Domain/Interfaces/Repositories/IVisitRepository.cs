﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IVisitRepository<T> : IRepository<T> where T : class
    {
        Task<int> GetPatientVisitsCountByIdAsync(int patientId);
        Task<IEnumerable<Visit>> GetPatientVisitsByIdAsync(int patientId);
    }
}
