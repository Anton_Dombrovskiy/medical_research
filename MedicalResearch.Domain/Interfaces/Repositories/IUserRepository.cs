﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IUserRepository<T> : IRepository<T> 
        where T : class
    {
        User GetUserByLoginAndPassword(string login, string password);
        Task<bool> IsUserExistsWithSpecificLoginAsync(string login);
        User GetUserByLogin(string login);
        User GetUserByIdWithRole(int id);
        Task<IEnumerable<User>> GetAllUsersWithRoleAsync();
    }
}
