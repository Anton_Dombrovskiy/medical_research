﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface ISupplyRepository<T> : IRepository<T> 
        where T : class
    {
        Task<IEnumerable<T>> GetAllAsync(bool isSupplied);
    }
}
