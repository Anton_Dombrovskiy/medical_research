﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IRoleRepository<T> : IRepository<T>
           where T : class
    {
        Task<Role> GetRoleByNameAsync(string name);
    }
}
