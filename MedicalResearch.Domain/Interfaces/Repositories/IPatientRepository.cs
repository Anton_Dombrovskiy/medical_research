﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IPatientRepository<T> : IRepository<T> where T : class
    {
        Task<bool> IsPatientExistAsync(string patientNumber);
        IEnumerable<T> GetAllWithSpecificClinicId(int clinicId);
        T GetPatientByIdWithSpecificClinicId(int patientId, int clinicId);
        Task<int> GetAllRecordsCountAsync();
    }
}
