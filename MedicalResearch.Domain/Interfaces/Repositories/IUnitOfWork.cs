﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<ResearchProject> ResearchProject { get; }
        IClinicRepository<Clinic> Clinic { get; }
        IMedicineRepository<Medicine> Medicine { get; }
        IPatientRepository<Patient> Patient { get; }
        IUserRepository<User> User { get; }
        ISupplyRepository<Supply> Supply { get; }
        IVisitRepository<Visit> Visit { get; }
        IUniqueClinicAndMedecineIdsRepository<UniqueClinicAndMedecineIds> UniqueClinicAndMedecineIdsRepository { get; }
        IRoleRepository<Role> Role { get; }
        Task SaveAsync();
    }
}
