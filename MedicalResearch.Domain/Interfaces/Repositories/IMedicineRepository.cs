﻿using MedicalResearch.Domain.Models.Pagination;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Repositories
{
    public interface IMedicineRepository<T> : IRepository<T> 
        where T: class 
    {
        Task<int> GetAllRecordsCountAsync();
        public IEnumerable<T> GetAllWithSpecificType(char type);
    }
}
