﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Interfaces.Validation
{
    /// <summary>
    /// The interface provides main methods to validate user account
    /// </summary>
    public interface IUserValidation : IEmailValidation
    {
        bool ValidateUserInputData(User user);

        /// <summary>
        /// The method indicates if the first name in correct format
        /// </summary>
        /// <param name="firstName">The first name of user</param>
        /// <returns>If the first name in correct format</returns>
        bool ValidateFirstName(string firstName);

        /// <summary>
        /// The method indicates if the initials in correct format
        /// </summary>
        /// <param name="initials">The initials of user</param>
        /// <returns>If the initials in correct format</returns>
        bool ValidateLastName(string lastName);

        /// <summary>
        /// The method indicates if the last name in correct format
        /// </summary>
        /// <param name="lastName">The last name of user</param>
        /// <returns>If the last name in correct format</returns>
        bool ValidateInitials(string initials);

        /// <summary>
        /// The method indicates if the password in correct format
        /// </summary>
        /// <param name="password">The password of user</param>
        /// <returns>If the password in correct format</returns>
        bool ValidatePassword(string password);
    }
}
