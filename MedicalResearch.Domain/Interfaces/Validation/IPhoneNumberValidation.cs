﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Validation
{
    public interface IPhoneNumberValidation
    {
        bool ValidatePhoneNumber(string phoneNumber);
    }
}
