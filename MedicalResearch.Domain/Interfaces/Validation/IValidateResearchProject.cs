﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Validation
{
    /// <summary>
    /// The interface provides methods to validate new news about research project
    /// </summary>
    public interface IValidateResearchProject
    {
        /// <summary>
        /// The method validates if the date is correct
        /// </summary>
        /// <param name="dateOfNews">The date of news publishing</param>
        /// <returns>If date of news is valid</returns>
        bool ValidateDateOfNews(DateTime dateOfNews);

        /// <summary>
        /// The method validate if title in correct format
        /// </summary>
        /// <param name="title">The title of news</param>
        /// <returns>If title in correct format</returns>
        bool ValidateTitle(string title);

        /// <summary>
        /// The method validates if detailed information in correct format
        /// </summary>
        /// <param name="detailedInfo">The detailed information about news</param>
        /// <returns>If detailed information in correct format</returns>
        bool ValidateDetailedInformation(string detailedInfo);
    }
}
