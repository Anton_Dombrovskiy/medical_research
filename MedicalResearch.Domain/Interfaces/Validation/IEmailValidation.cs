﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Validation
{
    /// <summary>
    /// The interface provides method to validate email
    /// </summary>
    public interface IEmailValidation
    {
        /// <summary>
        /// The method indecates if the email is valid
        /// </summary>
        /// <param name="email">The email address</param>
        /// <returns>If email is valid</returns>
        bool ValidateEmail(string email);
    }
}
