﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Validation
{
    /// <summary>
    /// The interface provides methods to validate medicine instance
    /// </summary>
    public interface IMedicineTypeValidation
    {
        string GetPossibleMedicineTypes();
        bool ValidateMedicineType(char type);
    }
}
