﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Validation
{
    /// <summary>
    /// The interface provides methods to validate patient
    /// </summary>
    public interface IPatientValidation
    {
        /// <summary>
        /// The method indicates if the date of birthday is valid
        /// </summary>
        /// <param name="dateOfBirth">The date of birthday</param>
        /// <returns>If the date of birthday is valid</returns>
        bool ValidateDateOfBirthday(DateTime dateOfBirth);

        public bool ValidateNumber(string number, out int clinicId, out int numbers);
    }
}
