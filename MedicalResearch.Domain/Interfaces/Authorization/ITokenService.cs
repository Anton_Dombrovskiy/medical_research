﻿using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Interfaces.Authorization
{
    public interface ITokenService
    {
        string BuildAccessToken(string key, string issuer, User user);
        string BuildRefreshToken(string key, string issuer, User user);
        bool ValidateToken(string key, string issuer, string token);
    }
}
