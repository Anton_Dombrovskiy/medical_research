﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.DAL.EF
{
    public class MedicalResearchContext : DbContext
    {
        public DbSet<ResearchProject> ResearchProjects { get; set; }
        public DbSet<Clinic> Clinics { get; set; }
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Supply> Supplies { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<UniqueClinicAndMedecineIds> uniqueClinicAndMedecineIds { get; set; }
        public DbSet<Role> Roles { get; set; }

        public MedicalResearchContext(DbContextOptions<MedicalResearchContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UniqueClinicAndMedecineIds>().HasData(
                new UniqueClinicAndMedecineIds
                {
                    Id = 1,
                    ClinicUniqueId = 100,
                    MedicineUniqueId = 100000
                });

            modelBuilder.Entity<Role>().HasData(
                new Role { Id = 1, Name = "Researcher" },
                new Role { Id = 2, Name = "Sponsor" },
                new Role { Id = 3, Name = "Manager" },
                new Role { Id = 4, Name = "Admin" }
                );

            modelBuilder.Entity<User>().HasData(
                new 
                {
                    Id = 1,
                    FirstName = "Admin",
                    LastName = "Admin",
                    Initials = "AA",
                    Email = "ng.medical.research@gmail.com",
                    Password = "AQAAAAEAACcQAAAAEJIB2Pot90z7BgaB5yi0V7oDjHhLdRpi3iezXCaVBT7YCQQLFkaM2TjSdsYgfumYHA==",
                    UserAvatarName = "default.png",
                    IsLocked = false,
                    RoleId = 4
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}
