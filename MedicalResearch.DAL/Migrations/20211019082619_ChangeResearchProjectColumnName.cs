﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalResearch.DAL.Migrations
{
    public partial class ChangeResearchProjectColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BackgroundImageURL",
                table: "ResearchProjects",
                newName: "BackgroundImageName");

            migrationBuilder.UpdateData(
                table: "uniqueClinicAndMedecineIds",
                keyColumn: "Id",
                keyValue: 1,
                column: "MedicineUniqueId",
                value: 100000);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BackgroundImageName",
                table: "ResearchProjects",
                newName: "BackgroundImageURL");

            migrationBuilder.UpdateData(
                table: "uniqueClinicAndMedecineIds",
                keyColumn: "Id",
                keyValue: 1,
                column: "MedicineUniqueId",
                value: 1000000);
        }
    }
}
