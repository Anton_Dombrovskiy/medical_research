﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalResearch.DAL.Migrations
{
    public partial class Addadminseed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserIcoURL",
                table: "Users",
                newName: "UserAvatarName");

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "AttachedClinic", "Email", "FirstName", "Initials", "IsLocked", "LastName", "Password", "RoleId", "UserAvatarName" },
                values: new object[] { 1, null, "ng.medical.research@gmail.com", "Admin", "AA", false, "Admin", "AQAAAAEAACcQAAAAEJIB2Pot90z7BgaB5yi0V7oDjHhLdRpi3iezXCaVBT7YCQQLFkaM2TjSdsYgfumYHA==", 4, "default.png" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.RenameColumn(
                name: "UserAvatarName",
                table: "Users",
                newName: "UserIcoURL");
        }
    }
}
