﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MedicalResearch.DAL.Migrations
{
    public partial class AddColumnInSupplyTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSupplied",
                table: "Supplies",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSupplied",
                table: "Supplies");
        }
    }
}
