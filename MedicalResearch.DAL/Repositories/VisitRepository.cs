﻿using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedicalResearch.DAL.Repositories
{
    class VisitRepository : Repository<Visit>, IVisitRepository<Visit>
    {
        public VisitRepository(MedicalResearchContext db) : base(db)
        { }

        public async Task<int> GetPatientVisitsCountByIdAsync(int patientId) => await db.Visits.Include(p=>p.Patient).Where(p=>p.Patient.Id == patientId).CountAsync();
        public async Task<IEnumerable<Visit>> GetPatientVisitsByIdAsync(int patientId) => await db.Visits.Include(m=>m.DispencedMedicine)
            .Include(p => p.Patient).ThenInclude(p=>p.Clinic).Where(v => v.Patient.Id == patientId).ToListAsync();
    }
}
