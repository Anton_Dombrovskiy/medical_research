﻿using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MedicalResearch.DAL.Repositories
{
    public abstract class Repository<T> : IRepository<T>
        where T : class
    {
        protected MedicalResearchContext db;

        protected Repository(MedicalResearchContext db)
        {
            this.db = db;
        }

        public async Task CreateAsync(T item) => await db.Set<T>().AddAsync(item);

        public async Task DeleteAsync(int id)
        {
            var entity = await GetAsync(id);
            if (entity != null)
            {
                db.Remove(entity);
            }
        }

        public async Task<T> GetAsync(int id) => await db.Set<T>().FindAsync(id);

        public async Task<IEnumerable<T>> GetAllAsync() => await db.Set<T>().ToListAsync();

        public void Update(T item) => db.Set<T>().Update(item);
    }
}
