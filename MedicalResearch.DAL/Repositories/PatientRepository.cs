﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.DAL.Repositories
{
    public class PatientRepository : Repository<Patient>, IPatientRepository<Patient>
    {
        public PatientRepository(MedicalResearchContext db) : base(db)
        { }

        public async Task<int> GetAllRecordsCountAsync() => await db.Patients.CountAsync();
        public IEnumerable<Patient> GetAllWithSpecificClinicId(int clinicId) => db.Patients.Include(c => c.Clinic).Where(p => p.Clinic.Id == clinicId);
        public async Task<bool> IsPatientExistAsync(string patientNumber) => await db.Patients.AnyAsync(p => p.PatientNumber == patientNumber);
        public Patient GetPatientByIdWithSpecificClinicId(int patientId, int clinicId) => db.Patients.Include(c => c.Clinic)
            .Where(p => p.Id == patientId && p.Clinic.Id == clinicId).FirstOrDefault();
    }
}
