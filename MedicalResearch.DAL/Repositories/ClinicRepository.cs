﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalResearch.DAL.Repositories
{
    public class ClinicRepository : Repository<Clinic>, IClinicRepository<Clinic>
    {
        public ClinicRepository(MedicalResearchContext db) : base(db)
        { }

        public async Task<Clinic> GetClinicByClinicNameAsync(string clinicName) => await db.Clinics.SingleOrDefaultAsync(c => c.Name == clinicName);
        public async Task<int> GetAllRecordsCountAsync() => await db.Clinics.CountAsync();
    }
}
