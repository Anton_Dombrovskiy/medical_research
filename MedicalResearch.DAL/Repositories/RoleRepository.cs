﻿using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.DAL.Repositories
{
    class RoleRepository : Repository<Role>, IRoleRepository<Role>
    {
        public RoleRepository(MedicalResearchContext context) : base(context)
        { }

        public async Task<Role> GetRoleByNameAsync(string name) => await db.Roles.Where(r => r.Name == name).FirstOrDefaultAsync();
    }
}
