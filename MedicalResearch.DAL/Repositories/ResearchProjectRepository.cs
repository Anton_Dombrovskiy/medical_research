﻿using System;
using System.Collections.Generic;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.DAL.Repositories
{
    public class ResearchProjectRepository : Repository<ResearchProject>
    {
        public ResearchProjectRepository(MedicalResearchContext db) : base(db)
        { }
    }
}
