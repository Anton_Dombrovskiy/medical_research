﻿using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Models;
using MedicalResearch.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.DAL.Repositories
{
    class UniqueClinicAndMedecineIdsRepository : Repository<UniqueClinicAndMedecineIds>, IUniqueClinicAndMedecineIdsRepository<UniqueClinicAndMedecineIds>
    {
        private const int indexOfTableWithUniqueIds = 1;
        public UniqueClinicAndMedecineIdsRepository(MedicalResearchContext db) : base(db)
        { }

        public async Task<int> GetUniqueClinicIdAsync()
        {
            var uniqueClinicandMedicineId = await GetAsync(indexOfTableWithUniqueIds);
            int uniqueClinicId = ++uniqueClinicandMedicineId.ClinicUniqueId;
            Update(uniqueClinicandMedicineId);
            await db.SaveChangesAsync();
            return uniqueClinicId;
        }

        public async Task<int> GetUniqueMedicineIdAsync()
        {
            var uniqueClinicandMedicineId = await GetAsync(indexOfTableWithUniqueIds);
            int uniqueMedicineId = ++uniqueClinicandMedicineId.MedicineUniqueId;
            Update(uniqueClinicandMedicineId);
            await db.SaveChangesAsync();
            return uniqueMedicineId;
        }
    }
}
