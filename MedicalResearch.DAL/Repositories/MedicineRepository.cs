﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalResearch.DAL.Repositories
{
    public class MedicineRepository : Repository<Medicine>, IMedicineRepository<Medicine>
    {
        public MedicineRepository(MedicalResearchContext db) : base(db)
        { }

        public IEnumerable<Medicine> GetAllWithSpecificType(char type) => db.Medicines.Where(m => m.Type == type);
        public async Task<int> GetAllRecordsCountAsync() => await db.Medicines.CountAsync();
    }
}