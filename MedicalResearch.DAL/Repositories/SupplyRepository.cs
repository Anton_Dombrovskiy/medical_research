﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalResearch.DAL.Repositories
{
    public class SupplyRepository : Repository<Supply>, ISupplyRepository<Supply>
    {
        public SupplyRepository(MedicalResearchContext db) : base(db)
        { }

        public async Task<IEnumerable<Supply>> GetAllAsync(bool isSupplied) => await db.Supplies.Include(x => x.Clinic).Include(x => x.Medicine).Where(s => s.IsSupplied == isSupplied).ToListAsync();
    }
}
