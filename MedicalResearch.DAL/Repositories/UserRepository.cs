﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace MedicalResearch.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository<User>
    {
        public UserRepository(MedicalResearchContext db) : base(db)
        { }

        public async Task<bool> IsUserExistsWithSpecificLoginAsync(string login) => await db.Users.AnyAsync(u => u.Email == login);
        public User GetUserByLoginAndPassword(string login, string password)
        {

            return db.Users.Include(r => r.Role)
            .Where(user => user.Email == login &&
                        user.Password == password).SingleOrDefault();
        }

        public User GetUserByIdWithRole(int id) => db.Users.Include(r => r.Role).Where(user => user.Id == id).FirstOrDefault();
        public async Task<IEnumerable<User>> GetAllUsersWithRoleAsync() => await db.Users.Include(r => r.Role).ToListAsync();
        public User GetUserByLogin(string login) => db.Users.Include(r=>r.Role).Where(u => u.Email == login).FirstOrDefault();

    }
}
