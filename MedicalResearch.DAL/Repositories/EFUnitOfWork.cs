﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.DAL.EF;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly MedicalResearchContext db;
        private readonly ResearchProjectRepository researchProjectRepository;
        private readonly UserRepository userRepository;
        private readonly ClinicRepository clinicRepository;
        private readonly MedicineRepository medicineRepository;
        private readonly PatientRepository patientRepository;
        private readonly SupplyRepository supplyRepository;
        private readonly VisitRepository visitRepository;
        private readonly UniqueClinicAndMedecineIdsRepository uniqueClinicAndMedecineIdsRepository;
        private readonly RoleRepository roleRepository;
        public EFUnitOfWork(MedicalResearchContext context)
        {
            db = context;
        }

        public IRepository<ResearchProject> ResearchProject => researchProjectRepository is null ? new ResearchProjectRepository(db) : researchProjectRepository;
        public IClinicRepository<Clinic> Clinic => clinicRepository is null ? new ClinicRepository(db) : clinicRepository;
        public IMedicineRepository<Medicine> Medicine => medicineRepository is null ? new MedicineRepository(db) : medicineRepository;
        public IPatientRepository<Patient> Patient => patientRepository is null ? new PatientRepository(db) : patientRepository;
        public IUserRepository<User> User => userRepository is null ? new UserRepository(db) : userRepository;
        public ISupplyRepository<Supply> Supply => supplyRepository is null ? new SupplyRepository(db) : supplyRepository;
        public IVisitRepository<Visit> Visit => visitRepository is null ? new VisitRepository(db) : visitRepository;
        public IUniqueClinicAndMedecineIdsRepository<UniqueClinicAndMedecineIds> UniqueClinicAndMedecineIdsRepository =>
            uniqueClinicAndMedecineIdsRepository is null ? new UniqueClinicAndMedecineIdsRepository(db) : uniqueClinicAndMedecineIdsRepository;

        public IRoleRepository<Role> Role => roleRepository is null ? new RoleRepository(db) : roleRepository;

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task SaveAsync()
        {
           await this.db.SaveChangesAsync();
        }
    }
}
