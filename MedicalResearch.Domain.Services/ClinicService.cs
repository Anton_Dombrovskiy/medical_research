﻿using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Services
{
    public class ClinicService : IClinicService
    {
        private IUnitOfWork _uof;
        public ClinicService(IUnitOfWork unitOfWork)
        {
            _uof = unitOfWork;
        }

        public async Task AddClinicAsync(Clinic clinic)
        {
            clinic.Id = await _uof.UniqueClinicAndMedecineIdsRepository.GetUniqueClinicIdAsync();

            await _uof.Clinic.CreateAsync(clinic);
            await _uof.SaveAsync();
        }
    }
}
