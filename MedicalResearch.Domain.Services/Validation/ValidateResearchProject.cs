﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.Interfaces.Validation;

namespace MedicalResearch.Domain.Services.Validation
{
    /// <summary>
    /// The class validates the news about research project
    /// </summary>
    public class ValidateResearchProject : IValidateResearchProject
    {
        /// <inheritdoc/>
        public bool ValidateDateOfNews(DateTime dateOfNews) => dateOfNews <= DateTime.Now;

        /// <inheritdoc/>
        public bool ValidateDetailedInformation(string detailedInfo) => !(string.IsNullOrWhiteSpace(detailedInfo) && detailedInfo.Length < 10);

        /// <inheritdoc/>
        public bool ValidateTitle(string title) => !(string.IsNullOrWhiteSpace(title) && title.Length < 2);
    }
}
