﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.Interfaces.Validation;

namespace MedicalResearch.Domain.Services.Validation
{
    /// <summary>
    /// The class validates the medicine instance
    /// </summary>
    public class MedicineTypeValidation : IMedicineTypeValidation
    {
        private const string LetterOfPossibleMedicineType = "ABCDEF";

        public string GetPossibleMedicineTypes() => LetterOfPossibleMedicineType;

        public bool ValidateMedicineType(char type) => LetterOfPossibleMedicineType.Contains(type, StringComparison.InvariantCultureIgnoreCase);
    }
}
