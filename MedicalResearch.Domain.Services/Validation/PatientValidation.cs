﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.Interfaces.Validation;

namespace MedicalResearch.Domain.Services.Validation
{
    /// <summary>
    /// The class validates the Patient
    /// </summary>
    public class PatientValidation : IPatientValidation
    {
        /// <inheritdoc/>
        public bool ValidateDateOfBirthday(DateTime dateOfBirth) => (DateTime.Now.Year - dateOfBirth.Year) >= 18;

        /// <inheritdoc/>
        public bool ValidateNumber(string number, out int clinicId, out int patientNumber)
        {
            clinicId = patientNumber = -1;
            int requireClinicLength = 3;
            int requirePatientNumberLength = 4;
            if (!string.IsNullOrEmpty(number))
            {
                string[] clinicIdAndPatientNumber = number.Split("-", 2);
                int clinicIdIndex = 0;
                int patientNumberIndex = 1;
                if (clinicIdAndPatientNumber.Length != 2 || string.IsNullOrEmpty(clinicIdAndPatientNumber[clinicIdIndex]) || string.IsNullOrEmpty(clinicIdAndPatientNumber[patientNumberIndex]))
                {
                    return false;
                }

                string clinicIdStr = clinicIdAndPatientNumber[clinicIdIndex];
                string patientNumberStr = clinicIdAndPatientNumber[patientNumberIndex];
                if (clinicIdStr.Length != requireClinicLength || patientNumberStr.Length != requirePatientNumberLength)
                {
                    return false;
                }

                if (int.TryParse(clinicIdStr, out clinicId) && int.TryParse(patientNumberStr, out patientNumber))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
