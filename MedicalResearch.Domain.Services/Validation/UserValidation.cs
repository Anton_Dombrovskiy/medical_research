﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Services.Validation
{
    /// <summary>
    /// The class validates the new user account
    /// </summary>
    public class UserValidation : IUserValidation
    {
        public bool ValidateUserInputData(User user)
        {
            if (!ValidateEmail(user.Email))
            {
                throw new ArgumentException("Invalid email.");
            }

            if (!ValidateFirstName(user.FirstName))
            {
                throw new ArgumentException("First name should contain letters only.");
            }

            if (!ValidateInitials(user.Initials))
            {
                throw new ArgumentException("Initials should contain letters only");
            }

            if (!ValidateLastName(user.LastName))
            {
                throw new ArgumentException("Last name should contain letters only.");
            }

            if (!ValidatePassword(user.Password))
            {
                throw new ArgumentException("Password should be minimum 8 symbols long and contains letters and numbers");
            }

            return true;
        }
        /// <inheritdoc/>
        public bool ValidateEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public bool ValidateFirstName(string firstName) => !(string.IsNullOrEmpty(firstName) && firstName.Any(ch => !char.IsLetter(ch)));

        /// <inheritdoc/>
        public bool ValidateInitials(string initials) => !(string.IsNullOrEmpty(initials) || initials.Length != 2 || initials.Any(ch => !char.IsLetter(ch)));

        /// <inheritdoc/>
        public bool ValidateLastName(string lastName) => !(string.IsNullOrEmpty(lastName) && lastName.Any(ch => !char.IsLetter(ch)));

        /// <inheritdoc/>
        public bool ValidatePassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password) && password.Length < 8)
            {
                return false;
            }

            return password.Any(ch => char.IsDigit(ch)) && password.Any(ch => char.IsLetter(ch));
        }
    }
}
