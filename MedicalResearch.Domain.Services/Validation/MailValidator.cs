﻿using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Models.Mail;
using System.Linq;
using System.Text.RegularExpressions;

namespace MedicalResearch.Domain.Services.Validation
{
    public class MailValidator : IMailValidator
    {
        private const string emailPattern = @"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
        public bool ValidateEmail(string email)
        {
            Regex validEmailRegex = new Regex(emailPattern, RegexOptions.IgnoreCase);
            return validEmailRegex.Match(email).Success;
        }

        public bool ValidatePhoneNumber(string phoneNumber)
        {
            return !phoneNumber.Any(char.IsLetter);
        }

        public bool ValidateContactEmailAndPhone(ContactEmail email)
        {
            return ValidateEmail(email.Email) && string.IsNullOrEmpty(email.PhoneNumber) ? true : ValidatePhoneNumber(email.PhoneNumber);
        }
    }
}
