﻿using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Services
{
    public class ProjectService : IProjectService
    {
        private readonly string pathToProjectPhotos;
        private readonly string[] permittedExtensions = { ".png", ".jpg", "jpeg" };
        private readonly IUnitOfWork _uof;
        private const string defaultImgName = "default.jpg";

        public ProjectService(IUnitOfWork uof)
        {
            _uof = uof;
            pathToProjectPhotos = Path.Combine("static", "img", "researchProjectBackgrounds");
        }


        public async Task AddProjectAsync(ResearchProject project)
        {
            project.BackgroundImageName = project.BackgroundImageName ?? defaultImgName;
            await _uof.ResearchProject.CreateAsync(project);
            await _uof.SaveAsync();
        }

        public async Task UpdateProjectAsync(ResearchProject project, DetailedResearchProjectDTO updatedProject)
        {
            project.Title = updatedProject.Title;
            project.DateOfNews = updatedProject.DateOfNews;
            project.DetailedInformation = updatedProject.DetailedInformation;
            project.BackgroundImageName = updatedProject.BackgroundImageName ?? project.BackgroundImageName;
            _uof.ResearchProject.Update(project);
            await _uof.SaveAsync();
        }

        public async Task<string> UploadPhotoAsync(IFormFile uploadedFile)
        {
            if (uploadedFile is null) return defaultImgName;
            var ext = Path.GetExtension(uploadedFile.FileName).ToLowerInvariant();
            if (string.IsNullOrEmpty(ext) || !permittedExtensions.Contains(ext))
            {
                return defaultImgName;
            }

            string uniqueFileName = $"{Guid.NewGuid()}{ext}";
            string path = Path.Combine(pathToProjectPhotos, uniqueFileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(fileStream);
            }

            return uniqueFileName;
        }
    }
}
