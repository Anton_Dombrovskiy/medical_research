﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace MedicalResearch.Domain.Services
{
    public class AccountService : IAccountService
    {
        private readonly string PathToAvatarPhotos;
        private readonly string[] permittedExtensions = { ".png", ".jpg", "jpeg" };
        private readonly IUserValidation userValidation;
        private readonly IUnitOfWork _uof;
        private const string DefaultImgName = "default.png";

        public AccountService(IUserValidation userValidation, IUnitOfWork uof)
        {
            this.userValidation = userValidation;
            _uof = uof;
            PathToAvatarPhotos = Path.Combine("static", "img", "userAvatars");
        }

        public async Task UpdateAccountInformationAsync(User user, UpdateUserDTO updatedUser)
        {
            if (userValidation.ValidateFirstName(updatedUser.FirstName)
                && userValidation.ValidateLastName(updatedUser.LastName)
                && userValidation.ValidateInitials(updatedUser.Initials))
            {
                user.FirstName = $"{char.ToUpper(updatedUser.FirstName[0])}{updatedUser.FirstName[1..].ToLower()}";
                user.LastName = $"{char.ToUpper(updatedUser.LastName[0])}{updatedUser.LastName[1..].ToLower()}";
                user.Initials = updatedUser.Initials.ToUpper();
                user.UserAvatarName = string.IsNullOrWhiteSpace(updatedUser.UserAvatarName) ? user.UserAvatarName : updatedUser.UserAvatarName;
                if (!string.IsNullOrWhiteSpace(updatedUser.NewPassword) && userValidation.ValidatePassword(updatedUser.NewPassword))
                {
                    user.Password = new PasswordHasher<User>().HashPassword(user, updatedUser.NewPassword);
                }

                _uof.User.Update(user);
                await _uof.SaveAsync();
            }
        }

        public async Task UpdateSensitiveInformationAsync(User user, UserSensitiveDTO updatedUser)
        {
            if (int.TryParse(updatedUser.AttachedClinic, out var clinicId))
            {
                var clinic = await _uof.Clinic.GetAsync(clinicId);
                if (clinic is null)
                {
                    throw new ArgumentException($"Clinic with id: {updatedUser.AttachedClinic} doesn't exist.");
                }
            }

            user.Role = await _uof.Role.GetRoleByNameAsync(updatedUser.Role) ?? user.Role;
            user.IsLocked = updatedUser.IsLocked;
            user.AttachedClinic = updatedUser.AttachedClinic;

            _uof.User.Update(user);
            await _uof.SaveAsync();
        }

        public async Task RemoveUserAsync(int id)
        {
            var user = await _uof.User.GetAsync(id);
            if (user is null)
            {
                throw new ArgumentException($"User with id: {id} wasn't found");
            }

            user.IsLocked = true;
            _uof.User.Update(user);
            await _uof.SaveAsync();
        }

        public async Task AddUserAsync(User user)
        {
            userValidation.ValidateUserInputData(user);
            if (await _uof.User.IsUserExistsWithSpecificLoginAsync(user.Email))
            {
                throw new ArgumentException($"Account with email: {user.Email} already exists.");
            }

            var role = await _uof.Role.GetRoleByNameAsync("Researcher");
            user.FirstName = $"{char.ToUpper(user.FirstName[0])}{user.FirstName[1..].ToLower()}";
            user.LastName = $"{char.ToUpper(user.LastName[0])}{user.LastName[1..].ToLower()}";
            user.Initials = user.Initials.ToUpper();
            user.Role = role;
            user.UserAvatarName = DefaultImgName;
            user.Password = new PasswordHasher<User>().HashPassword(user, user.Password);

            await _uof.User.CreateAsync(user);
            await _uof.SaveAsync();
        }

        public async Task<string> UploadPhotoAsync(IFormFile uploadedFile)
        {
            if (uploadedFile is null) return DefaultImgName;
            var ext = Path.GetExtension(uploadedFile.FileName).ToLowerInvariant();
            if (string.IsNullOrEmpty(ext) || !permittedExtensions.Contains(ext))
            {
                return DefaultImgName;
            }

            string uniqueFileName = $"{Guid.NewGuid()}{ext}";
            string path = Path.Combine(PathToAvatarPhotos, uniqueFileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await uploadedFile.CopyToAsync(fileStream);
            }

            return uniqueFileName;
        }
    }
}