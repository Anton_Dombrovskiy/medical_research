﻿using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models.Mail;
using Microsoft.Extensions.Options;
using MimeKit;
using MailKit.Net.Smtp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Services
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;

        public MailService(IOptions<MailSettings> mailSettings)
        {
            _mailSettings = mailSettings.Value;
        }

        public async Task SendEmailAsync(ContactEmail email)
        {
            var emailMessage = new MimeMessage();
            emailMessage.Sender = MailboxAddress.Parse(_mailSettings.SenderMail);
            emailMessage.To.Add(MailboxAddress.Parse(_mailSettings.ReceiverMail));
            emailMessage.Subject = string.IsNullOrEmpty(email.Topic) ? "Without theme" : email.Topic;
            emailMessage.Body = BuildContactMailBody(email);
            using (var smpt = new SmtpClient())
            {
                await smpt.ConnectAsync(_mailSettings.Host, _mailSettings.Port, MailKit.Security.SecureSocketOptions.SslOnConnect);
                await smpt.AuthenticateAsync(_mailSettings.SenderMail, _mailSettings.Password);
                await smpt.SendAsync(emailMessage);
                smpt.Disconnect(true);
            }

        }

        private MimeEntity BuildContactMailBody(ContactEmail email)
        {
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody += $"<p>Name: {(string.IsNullOrEmpty(email.Name) ? "-" : email.Name)}</p>";
            bodyBuilder.HtmlBody += $"<p>Email: {(string.IsNullOrEmpty(email.Email) ? "-" : email.Email)}</p>";
            bodyBuilder.HtmlBody += $"<p>Phone number: {(string.IsNullOrEmpty(email.PhoneNumber) ? "-" : email.PhoneNumber)}</p>";
            bodyBuilder.HtmlBody += $"<p>Address: {(string.IsNullOrEmpty(email.Address) ? "-" : email.Address)}</p>";
            bodyBuilder.HtmlBody += $"<p>Message: {(string.IsNullOrEmpty(email.Message) ? "-" : email.Message)}</p>";
            return bodyBuilder.ToMessageBody();
        }
    }
}
