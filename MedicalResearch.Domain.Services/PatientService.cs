﻿using MedicalResearch.DAL.Repositories;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Services
{
    public class PatientService : IPatientService
    {
        private readonly IMedicineTypeValidation _typeValidation;
        private readonly IUnitOfWork _uof;
        private readonly IPatientValidation _patientValidation;
        private readonly Random randomValue;
        public PatientService(IUnitOfWork uof, IPatientValidation patientValidation, IMedicineTypeValidation typeValidation)
        {
            _uof = uof;
            _patientValidation = patientValidation;
            _typeValidation = typeValidation;
            randomValue = new Random();
        }

        public async Task<IEnumerable<MedicineNameDTO>> GetAllPatientDispensedMedicinesAsync(int patientId)
        {
            var patientVisits = await _uof.Visit.GetPatientVisitsByIdAsync(patientId);
            List<MedicineNameDTO> dispensedMedicines = new List<MedicineNameDTO>();
            foreach (var visit in patientVisits)
            {
                if (visit.DispencedMedicine != null)
                {
                    dispensedMedicines.Add(new MedicineNameDTO
                    {
                        Description = visit.DispencedMedicine.Description,
                        Id = visit.DispencedMedicine.ID
                    });
                }
            }

            return dispensedMedicines;
        }

        public async Task<IEnumerable<Patient>> GetAllPatientAsync(bool specifyClinic, string clinicId = null)
        {
            if (!specifyClinic)
            {
                return await _uof.Patient.GetAllAsync();
            }

            if (specifyClinic && int.TryParse(clinicId, out var id))
            {
                return _uof.Patient.GetAllWithSpecificClinicId(id);
            }
            else
            {
                throw new ArgumentException("Your clinic isn't specified");
            }
        }

        public async Task<Patient> AddPatientAsync(AddPatientDTO patient)
        {
            if (!_patientValidation.ValidateNumber(patient.PatientNumber, out var clinicId, out var numbers))
            {
                throw new ArgumentException("PatientNumber has incorrect format");
            }

            if (!_patientValidation.ValidateDateOfBirthday(patient.DateOfBirthday))
            {
                throw new ArgumentException("Patient age should be more or equal 18");
            }

            Clinic clinic = await _uof.Clinic.GetAsync(clinicId);
            if (clinic is null)
            {
                throw new ArgumentException($"Clinic with id: {clinicId} was not found");
            }

            if (await _uof.Patient.IsPatientExistAsync(patient.PatientNumber))
            {
                throw new ArgumentException("Patient already exist");
            }

            Patient _patient = new Patient
            {
                PatientNumber = patient.PatientNumber,
                DateOfBirthday = patient.DateOfBirthday,
                Clinic = clinic,
                Sex = patient.Sex,
                Status = PatientStatus.Screened,
                LastVisitDate = DateTime.Now,
            };

            await AddNewPatientIntoDB(_patient);
            return _patient;
        }

        public async Task EndParticipationAsync(int patientId)
        {
            var patient = await _uof.Patient.GetAsync(patientId);
            if (patient != null)
            {
                patient.Status = PatientStatus.Finished;
                _uof.Patient.Update(patient);
                await _uof.SaveAsync();
            }
        }

        public async Task PerformVisitAsync(int patientId)
        {
            var patientVisits = await _uof.Visit.GetPatientVisitsByIdAsync(patientId);
            if (patientVisits is null)
            {
                throw new ArgumentException($"Patient with id: {patientId} was not found");
            }

            Patient patient = patientVisits.First().Patient;
            if (patientVisits.Count() == 1)
            {
                patient.Status = PatientStatus.Randomized;
                patient.MedicineType = GetRandomMedicineType();
            }
            else if(patient.LastVisitDate.ToShortDateString() == DateTime.Now.ToShortDateString())
            {
                throw new ArgumentException($"You can perform visit only once a day");
            }

            var medicine = GetRandomMedicineBySpecificType(patient.MedicineType);
            Visit currentVisit = new Visit
            {
                DateOfVisit = DateTime.Now,
                DispencedMedicine = medicine,
                Patient = patient
            };

            patient.LastVisitDate = currentVisit.DateOfVisit;
            _uof.Patient.Update(patient);
            await _uof.Visit.CreateAsync(currentVisit);
            await _uof.SaveAsync();
        }

        private async Task AddNewPatientIntoDB(Patient patient)
        {
            Visit visit = new Visit
            {
                DateOfVisit = patient.LastVisitDate,
                Patient = patient,
                DispencedMedicine = null
            };

            await _uof.Patient.CreateAsync(patient);
            await _uof.Visit.CreateAsync(visit);
            await _uof.SaveAsync();
        }

        private char GetRandomMedicineType()
        {
            string possibleMedicineType = _typeValidation.GetPossibleMedicineTypes();
            return possibleMedicineType[randomValue.Next(0, possibleMedicineType.Length - 1)];
        }

        private Medicine GetRandomMedicineBySpecificType(char type)
        {
            var medicineArr =  _uof.Medicine.GetAllWithSpecificType(type).ToList();
            if (medicineArr.Count == 0)
            {
                throw new ArgumentException("There are no medicines to be despensed");
            }

            return medicineArr[randomValue.Next(0, medicineArr.Count - 1)];

        }
    }
}
