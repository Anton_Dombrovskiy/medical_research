﻿using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Services
{
    public class MedicineService : IMedicineService
    {
        private readonly IMedicineTypeValidation _typeValidation;
        private readonly IUnitOfWork _uof;
        public MedicineService(IUnitOfWork unitOfWork, IMedicineTypeValidation typeValidation)
        {
            _uof = unitOfWork;
            _typeValidation = typeValidation;
        }

        public async Task AddMedicineAsync(Medicine medicine)
        {
            if (!_typeValidation.ValidateMedicineType(medicine.Type))
            {
                throw new ArgumentException($"Invalid {nameof(medicine.Type)}. Possible types: {_typeValidation.GetPossibleMedicineTypes()}");
            }

            medicine.ID = await _uof.UniqueClinicAndMedecineIdsRepository.GetUniqueMedicineIdAsync();
            await _uof.Medicine.CreateAsync(medicine);
            await _uof.SaveAsync();
        }
    }
}
