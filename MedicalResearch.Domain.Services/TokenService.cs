﻿using MedicalResearch.Domain.Interfaces.Authorization;
using MedicalResearch.Domain.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MedicalResearch.Domain.Services
{
    public class TokenService : ITokenService
    {
        private const int TokenLifetimeMinute = 15;
        public string BuildAccessToken(string key, string issuer, User user)
        {
            string attachedClinic = user.Role.Name == "Researcher" ? user.AttachedClinic : "*";
            List<Claim> claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Role, user.Role.Name),
                new Claim("Clinic", attachedClinic ?? string.Empty)
            };

            return BuildToken(key, issuer, claims, TokenLifetimeMinute);
        }

        public string BuildRefreshToken(string key, string issuer, User user)
        {
            return BuildToken(key, issuer, null, 259200);
        }

        private string BuildToken(string key, string issuer, IEnumerable<Claim> claims, int lifeTimeInMinutes)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var tokenDescriptor = new JwtSecurityToken(issuer, issuer, claims, DateTime.UtcNow, 
                                                       DateTime.UtcNow.AddMinutes(lifeTimeInMinutes), credentials);
                        
            return new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);
        }

        public bool ValidateToken(string key, string issuer, string token)
        {
            var mySecret = Encoding.UTF8.GetBytes(key);
            var mySecurityKey = new SymmetricSecurityKey(mySecret);
            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                tokenHandler.ValidateToken(token,
                new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidIssuer = issuer,
                    IssuerSigningKey = mySecurityKey,
                }, out SecurityToken validatedToken);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
