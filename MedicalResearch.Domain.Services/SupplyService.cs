﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;

namespace MedicalResearch.Domain.Services
{
    public class SupplyService : ISupplyService
    {
        private IUnitOfWork _uof { get; }
        public SupplyService(IUnitOfWork unitOfWork)
        {
            _uof = unitOfWork;
        }


        public async Task<Supply> AddSupplyAsync(SupplyPostDTO supply)
        {
            Medicine medicine = await _uof.Medicine.GetAsync(supply.MedicineId);
            Clinic clinic = await _uof.Clinic.GetAsync(supply.ClinicId);
            if (medicine is null || clinic is null)
            {
                throw new ArgumentException($"Invalid {nameof(medicine)} or {nameof(clinic)} id");
            }

            if (supply.Amount <= 0)
            {
                throw new ArgumentException($"The amount can't be less or equals zero");
            }

            Supply _supply = new Supply
            {
                Amount = supply.Amount,
                Clinic = clinic,
                Medicine = medicine,
                IsSupplied = false
            };

            await _uof.Supply.CreateAsync(_supply);
            await _uof.SaveAsync();

            return _supply;
        }

        public async Task SupplyMedicineAsync()
        {
            var supplies = await _uof.Supply.GetAllAsync(isSupplied: false);
            foreach (var supply in supplies)
            {
                supply.IsSupplied = true;
                _uof.Supply.Update(supply);
            }

            await _uof.SaveAsync();
        }

        public async Task DeleteSupply(int id)
        {
            await _uof.Supply.DeleteAsync(id);
            await _uof.SaveAsync();
        }
    }
}
