﻿using AutoMapper;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medical_research.Automapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Patient mapping
            CreateMap<Patient, ShortPatientDTO>();
            CreateMap<Patient, PatientDTO>();
            CreateMap<Visit, VisitDTO>().ForMember("DispencedMedicine",
                opt => opt.MapFrom(m => $"{m.DispencedMedicine.ID}. {string.Join("", m.DispencedMedicine.Description.TakeWhile(ch => ch != '.'))},"));

            //Medicine mapping
            CreateMap<Medicine, MedicineDTO>();
            CreateMap<MedicineDTO, Medicine>();

            //Clinic mapping
            CreateMap<Clinic, ClinicDTO>();
            CreateMap<ClinicDTO, Clinic>();

            //Research project mapping
            CreateMap<ResearchProject, ShortResearchProjectDTO>();
            CreateMap<ResearchProject, DetailedResearchProjectDTO>();
            CreateMap<DetailedResearchProjectDTO, ResearchProject>();

            //Supply mapping
            CreateMap<Supply, SupplyDTO>().ForMember("MedicineName",
                opt => opt.MapFrom(s => string.Join("", s.Medicine.Description.TakeWhile(ch => ch != '.'))))
                                          .ForMember("ClinicName", opt => opt.MapFrom(s => s.Clinic.Name));
            CreateMap<Clinic, ClinicNameDTO>();
            CreateMap<Medicine, MedicineNameDTO>().ForMember("Description", opt => opt.MapFrom(s => string.Join("", s.Description.TakeWhile(ch => ch != '.'))));

            //User mapping
            CreateMap<RegistrationDTO, User>();
            CreateMap<User, UserAccountDTO>().ForMember("Role", opt => opt.MapFrom(u => u.Role.Name));
            CreateMap<User, UserInfoDTO>();
            CreateMap<UpdateUserDTO, User>();
            CreateMap<User, ShortUserDTO>().ForMember("Role", opt => opt.MapFrom(u => u.Role.Name)); 
            CreateMap<User, UserSensitiveDTO>().ForMember("Role", opt => opt.MapFrom(u => u.Role.Name));
        }
    }
}
