﻿using Medical_research.Policies.Requirement;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Medical_research.Policies.Handlers
{
    public class ClinicRequirementHandler : AuthorizationHandler<ClinicRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, ClinicRequirement requirement)
        {
            if (!context.User.IsInRole(requirement.ForRole))
            {
                context.Succeed(requirement);            
            }

            if (!context.User.HasClaim(c => c.Type == "Clinic"))
            {
                return Task.CompletedTask;
            }

            var clinicId = context.User.FindFirstValue("Clinic");
            if (string.IsNullOrWhiteSpace(clinicId) || clinicId.Length != requirement.ClinicIdLength)
            {
                return Task.CompletedTask;
            }

            if (requirement.ContainsOnlyDigit && !clinicId.Any(ch => !char.IsDigit(ch)))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
