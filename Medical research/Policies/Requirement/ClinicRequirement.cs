﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medical_research.Policies.Requirement
{
    public class ClinicRequirement : IAuthorizationRequirement
    {
        public int ClinicIdLength { get; }
        public bool ContainsOnlyDigit { get; }
        public string ForRole { get; }

        public ClinicRequirement(int clinicIdLength, bool containsOnlyDigit, string forRole)
        {
            ClinicIdLength = clinicIdLength;
            ContainsOnlyDigit = containsOnlyDigit;
            ForRole = forRole;
        }
    }
}
