﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Medical_research.ConfigureServicesExtensions
{
    public static class AddCorsConfiguration
    {
        public static IServiceCollection ConfigureCors(this IServiceCollection services, string trustedOrigin)
        {
            services.AddCors(opt =>
            {
                opt.AddDefaultPolicy(policy =>
                {
                    policy.WithOrigins(trustedOrigin)
                          .AllowAnyHeader()
                          .AllowCredentials()
                          .AllowAnyMethod()
                          .AllowCredentials();
                });
            });

            return services;
        }
    }
}
