﻿using MedicalResearch.DAL.EF;
using MedicalResearch.DAL.Repositories;
using MedicalResearch.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace Medical_research.ConfigureServicesExtensions
{
    public static class AddDbContextExtension
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var config = new StringBuilder(configuration["ConnectionStrings:MedsConnectionMssql"]);
            var conn = config.Replace("ENVPW", configuration["DB_PW"]).ToString();
            services.AddDbContext<MedicalResearchContext>(
               options => options.UseSqlServer(conn, b => b.MigrationsAssembly("MedicalResearch.DAL")));
            services.AddScoped<IUnitOfWork, EFUnitOfWork>();

            return services;
        }
    }
}
