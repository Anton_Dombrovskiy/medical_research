﻿using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Services;
using MedicalResearch.Domain.Services.Validation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medical_research.ConfigureServicesExtensions
{
    public static class AddBLServicesExtension
    {
        public static IServiceCollection AddBLServices(this IServiceCollection services)
        {
            services.AddScoped<IMedicineTypeValidation, MedicineTypeValidation>();
            services.AddScoped<IPatientValidation, PatientValidation>();
            services.AddScoped<IUserValidation, UserValidation>();

            services.AddScoped<IMedicineService, MedicineService>();
            services.AddScoped<IClinicService, ClinicService>();
            services.AddScoped<IPatientService, PatientService>();
            services.AddScoped<ISupplyService, SupplyService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IProjectService, ProjectService>();

            services.AddTransient<IMailService, MailService>();
            services.AddTransient<IMailValidator, MailValidator>();
            return services;
        } 
    }
}
