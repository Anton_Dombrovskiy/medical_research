using System.IO;
using MedicalResearch.DAL.EF;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Medical_research.ConfigureServicesExtensions;
using Medical_research.Policies.Requirement;
using Microsoft.AspNetCore.Authorization;
using Medical_research.Policies.Handlers;
using MedicalResearch.Domain.Models.Mail;
using Medical_research.Middlewares;
using NLog;

namespace Medical_research
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext(Configuration);
            services.AddBLServices();

            services.AddControllers();
            services.AddJWTAuthenticationService(Configuration);
            services.AddAuthorization(conf => conf.AddPolicy("hasClinic", policy => policy.Requirements.Add(new ClinicRequirement(3, true, "Researcher"))));
            services.AddSingleton<IAuthorizationHandler, ClinicRequirementHandler>();

            services.AddAutoMapper();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Medical_research", Version = "v1" });
            });

            services.ConfigureCors(Configuration["TrustedOrigin"]);
            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, MedicalResearchContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Medical_research v1"));
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            dbContext.Database.Migrate();

            app.UseHttpsRedirection();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "static", "img", "researchProjectBackgrounds")),
                RequestPath = "/research-project-backgrounds",

            });

            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, "static", "img", "userAvatars")),
                RequestPath = "/user-avatars",

            });

            app.UseStatusCodePages();
            app.UseRouting();
            app.UseCors();

            app.UseAuthentication();

            app.UseAuthorization();
            app.UseRequestLogging();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
