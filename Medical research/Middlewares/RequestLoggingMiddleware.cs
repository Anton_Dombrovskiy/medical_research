﻿using Medical_research.Attributes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using NLog;
using System.Linq;
using System.Threading.Tasks;

namespace Medical_research.Middlewares
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private static readonly Logger _logger;

        static RequestLoggingMiddleware() => _logger = LogManager.GetCurrentClassLogger();
        public RequestLoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var endpoint = context.GetEndpoint();
            if (endpoint != null)
            {
                var endpointMetadata = endpoint.Metadata;
                if (endpointMetadata.Any(meta => meta is LoggingAttribute)
                    && !endpointMetadata.Any(meta => meta is SkipLoggingAttribute))
                {
                    _logger.Info($"Endopoint: {endpoint?.DisplayName}");
                }
            }

            await _next(context);


        }
    }

    public static class RequestLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestLogging(this IApplicationBuilder builder) => builder.UseMiddleware<RequestLoggingMiddleware>();
    }
}
