﻿using AutoMapper;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Authorization;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using MedicalResearch.Domain.Interfaces.Services;
using Medical_research.Attributes;
using NLog;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Medical_research.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private static readonly Logger _logger;

        static AccountController()=> _logger = LogManager.GetCurrentClassLogger();

        private readonly IUnitOfWork _unitOfWork;
        private readonly ITokenService _tokenService;
        private readonly IConfiguration _configuration;
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;

        public AccountController(IUnitOfWork unitOfWork, ITokenService tokenService, IConfiguration configuration, IAccountService accountService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _tokenService = tokenService;
            _configuration = configuration;
            _accountService = accountService;
            _mapper = mapper;
        }

        // GET: api/<AccountController>
        [HttpGet]
        public async Task<IEnumerable<UserInfoDTO>> Get()
        {
            var users = await _unitOfWork.User.GetAllUsersWithRoleAsync();
            var usersDTO = _mapper.Map<IEnumerable<UserInfoDTO>>(users);
            return usersDTO;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("sensitive")]
        public async Task<IEnumerable<UserSensitiveDTO>> GetSensitive()
        {
            var users = (await _unitOfWork.User.GetAllUsersWithRoleAsync()).Where(u => u.Role.Name != "Admin");
            var usersDTO = _mapper.Map<IEnumerable<UserSensitiveDTO>>(users);
            return usersDTO;
        }

        // GET api/<AccountController>/5
        [HttpGet("{id}")]
        public ActionResult<UserAccountDTO> Get(int id)
        {
            var user = _unitOfWork.User.GetUserByIdWithRole(id);
            if (user is null)
            {
                _logger.Warn("User was not found by id");
                return NotFound();
            }

            var userDTO = _mapper.Map<UserAccountDTO>(user);
            return userDTO;
        }

        [HttpGet("user-info/{id}")]
        public ActionResult<ShortUserDTO> GetShortUserInfo(int id)
        {
            var user = _unitOfWork.User.GetUserByIdWithRole(id);
            if (user is null)
            {
                _logger.Warn("User was not found by id");
                return NotFound();
            }

            var userDTO = _mapper.Map<ShortUserDTO>(user);
            return userDTO;
        }

        [HttpGet("user-role/{id}")]
        public ActionResult<Role> GetUserRole(int id)
        {
            var user = _unitOfWork.User.GetUserByIdWithRole(id);
            if (user is null)
            {
                _logger.Warn("User was not found by id");
                return NotFound();
            }

            return user.Role;
        }

        [HttpGet("attached-clinic/{id}")]
        public async Task<ActionResult<string>> GetAttachedClinic(int id)
        {
            var user = await _unitOfWork.User.GetAsync(id);
            if (user is null)
            {
                _logger.Warn("User was not found by id");
                return NotFound();
            }

            return user.AttachedClinic;
        }

        [AllowAnonymous]
        [Route("Login")]
        [HttpPost]
        public IActionResult Login(LoginDTO user)
        {
            var _user = _unitOfWork.User.GetUserByLogin(user.Email);
            if (_user is null)
            {
                _logger.Warn("User was not found by login while attempting to log in");
                return Ok(new { msg = "We can't find user with such credentials" });
            }

            var hashedPassword = new PasswordHasher<User>().VerifyHashedPassword(_user, _user.Password, user.Password);
            if (hashedPassword == PasswordVerificationResult.Failed)
            {
                _logger.Warn("User password and enter password weren't same");
                return Ok(new { msg = "We can't find user with such credentials" });
            }

            if (_user.IsLocked)
            {
                _logger.Warn("User attempted to login with locked account");
                return Ok(new { msg = "Account is locked" });
            }

            return Authenticate(_user, user.Remember);
        }

        [AllowAnonymous]
        [Route("Register")]
        [HttpPost]
        public async Task<IActionResult> Register(RegistrationDTO user)
        {
            try
            {
                User _user = _mapper.Map<User>(user);
                await _accountService.AddUserAsync(_user);

                return Authenticate(_user, true);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to register a new account");
                return Ok(new { msg = ex.Message });
            }
        }

        private IActionResult Authenticate(User user, bool remember = false)
        {
            var generatedToken = _tokenService.BuildAccessToken(_configuration["Jwt:Key"], _configuration["Jwt:Issuer"], user);
            if (remember)
            {
                var refreshToken = _tokenService.BuildRefreshToken(user.Password, _configuration["Jwt:Issuer"], user);
                var cookieOptions = new CookieOptions()
                {
                    HttpOnly = true,
                    Expires = DateTimeOffset.UtcNow.AddMonths(6),
                    Secure = true,
                    SameSite = SameSiteMode.None
                };

                Response.Cookies.Append("refresh_token", refreshToken, cookieOptions);
            }

            return Ok(new { access_token = generatedToken, userId = user.Id });
        }


        [AllowAnonymous]
        [HttpPost("refresh-token/{id}")]
        public IActionResult RefreshToken(int id)
        {
            var user = _unitOfWork.User.GetUserByIdWithRole(id);
            if (user is null)
            {
                _logger.Warn("User was not found by id");
                return NotFound();
            }

            var refreshToken = Request.Cookies["refresh_token"];
            bool isValid = _tokenService.ValidateToken(user.Password, _configuration["Jwt:Issuer"], refreshToken);
            if (isValid)
            {
                return Authenticate(user);
            }

            _logger.Warn("Attempting to refresh token failed");
            return Ok(new { msg = "Invalid token" });
        }

        [AllowAnonymous]
        [HttpPost("revoke-token")]
        public IActionResult RevokeToken()
        {
            var cookieOptions = new CookieOptions()
            {
                HttpOnly = true,
                Expires = DateTimeOffset.UtcNow.AddDays(-1),
                Secure = true,
                SameSite = SameSiteMode.None
            };

            Response.Cookies.Append("refresh_token", "", cookieOptions);
            return Ok();
        }

        // PUT api/<AccountController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateUserDTO user)
        {
            if (id != user.Id)
            {
                _logger.Warn("Id for updating and id of user weren't same");
                return BadRequest();
            }

            var oldUser = _unitOfWork.User.GetUserByIdWithRole(id);
            if (oldUser is null)
            {
                _logger.Warn("User for updating with specific id wasn't found");
                return NotFound();
            }

            if (User.FindFirst(ClaimTypes.Email)?.Value != oldUser.Email)
            {
                _logger.Warn("User tried to update account data of another user");
                return Forbid();
            }

            try
            {
                await _accountService.UpdateAccountInformationAsync(oldUser, user);
                Authenticate(oldUser);
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to update account data");
                return BadRequest(new { msg = ex.Message });
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("update-sensitive/{id}")]
        public async Task<IActionResult> UpdateSensitiveInformation(int id, [FromBody] UserSensitiveDTO user)
        {
            if (id != user.Id)
            {
                _logger.Warn("Id for updating and id of user weren't same");
                return BadRequest();
            }

            var oldUser = _unitOfWork.User.GetUserByIdWithRole(id);
            if (oldUser is null)
            {
                _logger.Warn("User for updating with specific id wasn't found");
                return NotFound();
            }

            try
            {
                await _accountService.UpdateSensitiveInformationAsync(oldUser, user);
                Authenticate(oldUser);
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to update account-sensitive data");
                return BadRequest(new { msg = ex.Message });
            }
        }

        // DELETE api/<AccountController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _accountService.RemoveUserAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to delete user account");
                return NotFound(new { msg = ex.Message });
            }
        }

        [HttpPost("setAvatar")]
        public async Task<IActionResult> SetAvatar(IFormFile uploadedFile)
        {
            try
            {
                var fileName = await _accountService.UploadPhotoAsync(uploadedFile);
                return Ok(new { fileName = fileName });
            }
            catch (Exception e)
            {
                _logger.Error("Error occurred while attempting to save file on server");
                return BadRequest(new { msg = e.Message });
            }
        }
    }
}
