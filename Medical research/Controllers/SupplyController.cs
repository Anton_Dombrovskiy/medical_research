﻿using AutoMapper;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;

namespace Medical_research.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Manager, Admin")]
    [ApiController]
    public class SupplyController : ControllerBase
    {
        private static readonly Logger _logger;

        static SupplyController() => _logger = LogManager.GetCurrentClassLogger();

        private readonly IUnitOfWork _unitOfWork;
        private readonly ISupplyService _supplyService;
        private readonly IMapper _mapper;

        public SupplyController(IUnitOfWork unitOfWork, ISupplyService supplyService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _supplyService = supplyService;
            _mapper = mapper;
        }

        // GET: api/<SupplyController>
        [HttpGet]
        public async Task<IEnumerable<SupplyDTO>> Get()
        {
            var supplies = await _unitOfWork.Supply.GetAllAsync(isSupplied: false);
            var suppliesDTO = _mapper.Map<IEnumerable<SupplyDTO>>(supplies);

            return suppliesDTO;
        }

        // GET api/<SupplyController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SupplyDTO>> Get(int id)
        {
            var supply = await _unitOfWork.Supply.GetAsync(id);
            var supplyDTO = _mapper.Map<SupplyDTO>(supply);

            return supplyDTO;
        }

        // POST api/<SupplyController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SupplyPostDTO supply)
        {
            try
            {
                Supply _supply = await _supplyService.AddSupplyAsync(supply);
                return CreatedAtAction(nameof(Post), new { id = _supply.Id, _supply });
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to add supply");
                return BadRequest(ex.Message);
            }
        }

        // DELETE api/<SupplyController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
           await _supplyService.DeleteSupply(id);
        }

        [Route("SupplyMedicine")]
        [HttpPost]
        public async Task<IActionResult> SupplyMedicine()
        {
            await _supplyService.SupplyMedicineAsync();
            return NoContent();
        }

        [Route("GetAllClinicNames")]
        [HttpGet]
        public async Task<IEnumerable<ClinicNameDTO>> GetClinicNames()
        {
            var clinics = await _unitOfWork.Clinic.GetAllAsync();
            var clinicNames = _mapper.Map<IEnumerable<ClinicNameDTO>>(clinics);

            return clinicNames;
        }

        [Route("GetAllMedicineNames")]
        [HttpGet]
        public async Task<IEnumerable<MedicineNameDTO>> GetMedicineNames()
        {
            var medicines = await _unitOfWork.Medicine.GetAllAsync();
            var medicineNames = _mapper.Map<IEnumerable<MedicineNameDTO>>(medicines);

            return medicineNames;
        }
    }
}
