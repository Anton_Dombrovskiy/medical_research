﻿using AutoMapper;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Medical_research.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Sponsor, Admin")]
    [ApiController]
    public class ClinicController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClinicService _clinicService;
        private static readonly Logger _logger;
        private readonly IMapper _mapper;

        static ClinicController() => _logger = LogManager.GetCurrentClassLogger();

        public ClinicController(IUnitOfWork unitOfWork, IClinicService clinicService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _clinicService = clinicService;
            _mapper = mapper;
        }

        // GET: api/<ClinicController>
        [HttpGet]
        public async Task<IEnumerable<ClinicDTO>> Get()
        {
            var clinics = await _unitOfWork.Clinic.GetAllAsync();
            var clinicsDto = _mapper.Map<IEnumerable<ClinicDTO>>(clinics);

            return clinicsDto;
        }

        // GET api/<ClinicController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ClinicDTO>> Get(int id)
        {
            Clinic clinic = await _unitOfWork.Clinic.GetAsync(id);
            if (clinic is null)
            {
                _logger.Warn("Clinic was not found by id");
                return NotFound();
            }

            ClinicDTO clinicDto = _mapper.Map<ClinicDTO>(clinic);
            return clinicDto;
        }

        // POST api/<ClinicController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ClinicDTO clinic)
        {
            Clinic _clinic = _mapper.Map<Clinic>(clinic);
            await _clinicService.AddClinicAsync(_clinic);

            return CreatedAtAction(nameof(Post), new { id = _clinic.Id, _clinic });
        }

        // PUT api/<ClinicController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ClinicDTO clinic)
        {
            if (id != clinic.Id)
            {
                _logger.Warn("Id for updating and id of clinic weren't same");
                return BadRequest();
            }

            try
            {
                Clinic _clinic = _mapper.Map<Clinic>(clinic);
                _unitOfWork.Clinic.Update(_clinic);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                _logger.Error("Errors occurred while attempting to update clinic data");
                return NotFound();
            }

            return NoContent();
        }
    }
}
