﻿using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Interfaces.Validation;
using MedicalResearch.Domain.Models.Mail;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using NLog;

namespace Medical_research.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        private static readonly Logger _logger;

        static MailController() => _logger = LogManager.GetCurrentClassLogger();

        private readonly IMailService _mailService;
        private readonly IMailValidator _contactEmailValidator;

        public MailController(IMailService mailService, IMailValidator contactEmailValidator)
        {
            _mailService = mailService;
            _contactEmailValidator = contactEmailValidator;
        }

        [HttpPost("sendContactMail")]
        public async Task<IActionResult> SendContactEmail(ContactEmail email)
        {
            if (!_contactEmailValidator.ValidateContactEmailAndPhone(email))
            {
                _logger.Warn("Validation failed while attempting to send an email");
                return Ok(new { msg = "Email or phone number are invalid." });
            }

            await _mailService.SendEmailAsync(email);
            return Ok();
        }
    }
}
