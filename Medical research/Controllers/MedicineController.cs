﻿using AutoMapper;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NLog;

namespace Medical_research.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Manager, Sponsor, Admin")]
    [ApiController]
    public class MedicineController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMedicineService _medicineService;
        private static readonly Logger _logger;
        private readonly IMapper _mapper;

        static MedicineController() => _logger = LogManager.GetCurrentClassLogger();
        public MedicineController(IUnitOfWork unitOfWork, IMedicineService medicineService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _medicineService = medicineService;
            _mapper = mapper;
        }

        // GET: api/<MedicineController>
        [HttpGet]
        public async Task<IEnumerable<MedicineDTO>> Get()
        {
            var medicines = await _unitOfWork.Medicine.GetAllAsync();
            var medicinesDto = _mapper.Map<IEnumerable<MedicineDTO>>(medicines);
            return medicinesDto;
        }

        // GET api/<MedicineController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MedicineDTO>> Get(int id)
        {
            Medicine medicine = await _unitOfWork.Medicine.GetAsync(id);
            if (medicine is null)
            {
                return NotFound();
            }

            MedicineDTO medicineDto = _mapper.Map<MedicineDTO>(medicine);
            return medicineDto;
        }

        // POST api/<MedicineController>
        [Authorize(Roles = "Manager, Admin")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MedicineDTO medicine)
        {
            Medicine _medicine = _mapper.Map<Medicine>(medicine);
            try
            {
                await _medicineService.AddMedicineAsync(_medicine);
            }
            catch (Exception ex)
            {
                _logger.Error("Validation errors occurred while attempting to add medicine");
                return ValidationProblem(ex.Message);
            }


            return CreatedAtAction(nameof(Post), new { id = _medicine.ID, _medicine });
        }

        // PUT api/<MedicineController>/5
        [Authorize(Roles = "Manager, Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] MedicineDTO medicine)
        {
            if (id != medicine.ID)
            {
                _logger.Warn("Id for updating and id of medicine weren't same");
                return BadRequest();
            }

            try
            {
                Medicine _medicine = _mapper.Map<Medicine>(medicine);

                _unitOfWork.Medicine.Update(_medicine);
                await _unitOfWork.SaveAsync();
            }
            catch (Exception)
            {
                _logger.Error("Errors occurred while attempting to update medicine data");
                return BadRequest();
            }

            return NoContent();
        }

        // DELETE api/<MedicineController>/5
        [Authorize(Roles = "Manager, Admin")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _unitOfWork.Medicine.DeleteAsync(id);
                await _unitOfWork.SaveAsync();
                return Ok();
            }
            catch (Exception)
            {
                _logger.Error("Error occurred while attempting to delete medicine that has already used");
                return BadRequest(new { msg = "You can't delete this medicine, because it's already used." });
            }

        }
    }
}
