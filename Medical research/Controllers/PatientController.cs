﻿using AutoMapper;
using MedicalResearch.Domain.DTO;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Interfaces.Services;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using NLog;

namespace Medical_research.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Researcher, Sponsor, Admin")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPatientService _patientService;
        private static readonly Logger _logger;
        private readonly IMapper _mapper;

        static PatientController() => _logger = LogManager.GetCurrentClassLogger();

        public PatientController(IUnitOfWork unitOfWork, IPatientService patientService, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _patientService = patientService;
            _mapper = mapper;
        }

        // GET: api/<PatientController>
        [Authorize(Policy = "hasClinic")]
        [HttpGet]
        public async Task<IEnumerable<ShortPatientDTO>> Get()
        {
            IEnumerable<Patient> patients;
            if (User.IsInRole("Researcher"))
            {
                Claim clinicClaim = User.Claims.FirstOrDefault(c => c.Type == "Clinic");
                patients = await _patientService.GetAllPatientAsync(true, clinicClaim?.Value);
            }
            else
            {
                patients = await _patientService.GetAllPatientAsync(false);
            }

            var patientsDTO = _mapper.Map<IEnumerable<ShortPatientDTO>>(patients);
            foreach (var patient in patientsDTO)
            {
                patient.DispensedMedicines = (await _patientService.GetAllPatientDispensedMedicinesAsync(patient.Id)).ToList();
                patient.DispensedMedicines.ForEach(medicine => medicine.Description =
                string.Join("", medicine.Description.TakeWhile(ch => ch != '.')));

            }

            return patientsDTO;
        }

        // GET api/<PatientController>/5
        [Authorize(Roles = "Researcher, Admin", Policy = "hasClinic")]
        [HttpGet("{id}")]
        public async Task<ActionResult<PatientDTO>> Get(int id)
        {
            var patient = await _unitOfWork.Patient.GetAsync(id);
            if (patient is null)
            {
                _logger.Warn("Patient was not found by id");
                return NotFound();
            }

            var patientDTO = _mapper.Map<PatientDTO>(patient);
            return patientDTO;
        }

        // POST api/<PatientController>
        [Authorize(Roles = "Researcher, Admin", Policy = "hasClinic")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AddPatientDTO patient)
        {
            Patient _patient;
            try
            {
                _patient = await _patientService.AddPatientAsync(patient);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to add patient");
                return BadRequest(new { msg = ex.Message });
            }

            return CreatedAtAction(nameof(Post), new { id = _patient.Id, _patient });
        }


        [Route("PerformVisit/{id}")]
        [Authorize(Roles = "Researcher, Admin", Policy = "hasClinic")]
        [HttpPost]
        public async Task<IActionResult> PerformVisit(int id)
        {
            try
            {
                Patient patient = await _unitOfWork.Patient.GetAsync(id);
                if (patient is null)
                {
                    throw new ArgumentException("Patient not found or attached to another clinic. Your clinic id: ");
                }

               await _patientService.PerformVisitAsync(id);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to perform patient visit");
                return BadRequest(new { msg = ex.Message });
            }

            return Ok();
        }

        [Authorize(Roles = "Researcher, Admin", Policy = "hasClinic")]
        [Route("GetPatientVisits/{id}")]
        [HttpGet]
        public async Task<IEnumerable<VisitDTO>> GetPatientVisits(int id)
        {
            var visits = await _unitOfWork.Visit.GetPatientVisitsByIdAsync(id);
            var visitsDTO = _mapper.Map<IEnumerable<VisitDTO>>(visits);

            return visitsDTO;
        }

        [Authorize(Roles = "Researcher, Admin", Policy = "hasClinic")]
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
           await _patientService.EndParticipationAsync(id);
        }
    }
}
