﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MedicalResearch.Domain.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MedicalResearch.Domain.Interfaces.Repositories;
using MedicalResearch.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MedicalResearch.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using NLog;

namespace Medical_research.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Admin, Sponsor")]
    [ApiController]
    public class ResearchProjectController : ControllerBase
    {
        private static readonly Logger _logger;

        static ResearchProjectController() => _logger = LogManager.GetCurrentClassLogger();

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProjectService _projectService;
        public ResearchProjectController(IUnitOfWork unitOfWork, IMapper mapper, IProjectService projectService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _projectService = projectService;
        }

        // GET: api/<ResearchProjectController>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IEnumerable<ShortResearchProjectDTO>> Get()
        {
            var researchProjects = await _unitOfWork.ResearchProject.GetAllAsync();
            var shortResearchProjects = _mapper.Map<IEnumerable<ShortResearchProjectDTO>>(researchProjects);

            return shortResearchProjects;
        }

        [HttpGet("detailed")]
        public async Task<IEnumerable<DetailedResearchProjectDTO>> GetDetailed()
        {
            var researchProjects = await _unitOfWork.ResearchProject.GetAllAsync();
            var shortResearchProjects = _mapper.Map<IEnumerable<DetailedResearchProjectDTO>>(researchProjects);

            return shortResearchProjects;
        }

        // GET api/<ResearchProjectController>/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<DetailedResearchProjectDTO>> Get(int id)
        {
            ResearchProject researchProject = await _unitOfWork.ResearchProject.GetAsync(id);
            if (researchProject is null)
            {
                _logger.Warn("Research project was not found by id");
                return NotFound();
            }

            DetailedResearchProjectDTO detailedResearchProject = _mapper.Map<DetailedResearchProjectDTO>(researchProject);
            return detailedResearchProject;
        }

        // POST api/<ResearchProjectController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DetailedResearchProjectDTO detailedResearchProject)
        {
            try
            {
                ResearchProject researchProject = _mapper.Map<ResearchProject>(detailedResearchProject);
                await _projectService.AddProjectAsync(researchProject);
                return CreatedAtAction(nameof(Post), new { id = researchProject.Id, researchProject });
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to add research project");
                return BadRequest(new { msg = ex.Message });
            }

        }

        // PUT api/<ResearchProjectController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] DetailedResearchProjectDTO detailedResearchProject)
        {
            if (id != detailedResearchProject.Id)
            {
                _logger.Warn("Id for updating and id of research project weren't same");
                return BadRequest();
            }

            var oldProject = await _unitOfWork.ResearchProject.GetAsync(id);
            if (oldProject is null)
            {
                _logger.Warn("Research project for updating with specific id wasn't found");
                return NotFound();
            }

            try
            {
                await _projectService.UpdateProjectAsync(oldProject, detailedResearchProject);
                return NoContent();
            }
            catch (Exception)
            {
                _logger.Error("Error occurred while attempting to update research project data");
                return NotFound();
            }
        }

        // DELETE api/<ResearchProjectController>/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _unitOfWork.ResearchProject.DeleteAsync(id);
            await _unitOfWork.SaveAsync();
        }

        [Route("saveProjectBackground")]
        [HttpPost]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            try
            {
                var fileName = await _projectService.UploadPhotoAsync(uploadedFile);
                return Ok(new { fileName });
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while attempting to save file on server");
                return BadRequest(new { msg = ex.Message });
            }
        }
    }
}
