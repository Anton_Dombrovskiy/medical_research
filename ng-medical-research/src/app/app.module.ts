import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RootModule } from './modules/root/root.module';
import { CoreModule } from './modules/core/core.module';

@NgModule({
  imports: [
    BrowserModule,
    CoreModule,
    RootModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
})
export class AppModule {}
