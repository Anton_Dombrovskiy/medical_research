import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoutesConfig } from './configs/routes.config';

const routes: Routes = [
  {
    path: RoutesConfig.basePaths.auth,
    loadChildren: () =>
      import('./modules/auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: RoutesConfig.basePaths.medicine,
    loadChildren: () =>
      import('./modules/medicine/medicine.module').then(
        (m) => m.MedicineModule
      ),
  },
  {
    path: RoutesConfig.basePaths.supply,
    loadChildren: () =>
      import('./modules/supply/supply.module').then((m) => m.SupplyModule),
  },
  {
    path: RoutesConfig.basePaths.clinic,
    loadChildren: () =>
      import('./modules/clinic/clinic.module').then((m) => m.ClinicModule),
  },
  {
    path: RoutesConfig.basePaths.research,
    loadChildren: () =>
      import('./modules/research/research.module').then(
        (m) => m.ResearchModule
      ),
  },
  {
    path: RoutesConfig.basePaths.project,
    loadChildren: () =>
      import('./modules/project/project.module').then((p) => p.ProjectModule),
  },
  {
    path: RoutesConfig.basePaths.account,
    loadChildren: () =>
      import('./modules/user/user.module').then((m) => m.UserModule),
  },
  { path: '', redirectTo: RoutesConfig.routes.home, pathMatch: 'full' },
  { path: '**', redirectTo: RoutesConfig.routes.error404 },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      relativeLinkResolution: 'legacy',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
