export interface ContactModel {
  name?: string;
  email: string;
  phoneNumber?: string;
  address?: string;
  topic?: string;
  message: string;
}
