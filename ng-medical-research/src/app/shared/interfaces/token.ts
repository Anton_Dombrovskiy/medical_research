export interface TokenJWT {
  access_token: string;
  userId: number;
}
