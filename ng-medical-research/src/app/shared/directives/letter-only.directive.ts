import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Directive({
  selector: '[appLetterOnly]',
  providers: [FormControl],
})
export class LetterOnlyDirective {
  constructor(private elem: ElementRef) {}
  @Input() OnlyLetters: boolean = true;
  @HostListener('input', ['$event.target.value']) onInput(
    formInputString: any
  ) {
    let onlyLettersString = formInputString.replace(/[^a-zA-Z]/g, '');
    this.elem.nativeElement.value = onlyLettersString;
  }
}
