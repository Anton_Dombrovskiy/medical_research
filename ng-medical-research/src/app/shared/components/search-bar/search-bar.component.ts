import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css'],
})
export class SearchBarComponent implements OnInit {
  searchString = '';
  constructor() {}
  textInput() {
    this.searchEvent.next(this.searchString);
  }

  ngOnInit(): void {}
  @Output() searchEvent = new EventEmitter<string>();
}
