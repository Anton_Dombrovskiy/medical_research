import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { IEntity } from '../../interfaces/entity';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent<T extends IEntity> implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  unsorted(a: any, b: any): number {
    return 0;
  }

  edit(item: T) {
    if (this.EnableModify) {
      this.editEvent.next(item);
    }
  }

  delete(id: number | string) {
    if (this.EnableDelete && confirm('Confirm acntion')) {
      this.deleteEvent.next(id);
    }
  }

  info(id: number | string) {
    if (this.EnableMoreInfo) {
      this.moreInfoEvent.next(id);
    }
  }

  isDate(value: any) {
    return value instanceof Date;
  }

  @Input() tableHeaders: string[] = [];
  @Input() tSource: T[] = [];
  @Input() EnableDelete: boolean = false;
  @Input() EnableModify: boolean = false;
  @Input() EnableMoreInfo: boolean = false;
  @Input() NumberOfPropertiesToSkip: number = 0;
  @Input() MakeEnumeration: boolean = false;
  @Output() deleteEvent = new EventEmitter<string | number>();
  @Output() editEvent = new EventEmitter<T>();
  @Output() moreInfoEvent = new EventEmitter<string | number>();
}
