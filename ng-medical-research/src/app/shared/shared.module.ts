import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TableComponent } from './components/table/table.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { LetterOnlyDirective } from './directives/letter-only.directive';

@NgModule({
  declarations: [TableComponent, SearchBarComponent, LetterOnlyDirective],
  imports: [CommonModule, ReactiveFormsModule, RouterModule, FormsModule],
  exports: [
    CommonModule,
    TableComponent,
    SearchBarComponent,
    LetterOnlyDirective,
  ],
})
export class SharedModule {}
