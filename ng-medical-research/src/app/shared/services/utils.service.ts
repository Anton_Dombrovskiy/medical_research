import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor() {}

  dateToShortStringConverter(date: Date | string) {
    if (typeof date !== 'string') {
      return `${date.getFullYear()}/${date.getDate()}/${date.getMonth() + 1}`;
    }

    return date;
  }
}
