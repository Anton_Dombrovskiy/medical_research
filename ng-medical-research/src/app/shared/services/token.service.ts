import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth_jwt_tokenKey';
const USERID_KEY = 'auth_userId_key';

@Injectable({
  providedIn: 'root',
})
export class TokenService {
  constructor() {}

  removeToken(): void {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.removeItem(USERID_KEY);
  }

  saveToken(token: string, userId: number) {
    window.localStorage.removeItem(TOKEN_KEY);
    window.localStorage.removeItem(USERID_KEY);
    window.localStorage.setItem(TOKEN_KEY, token);
    window.localStorage.setItem(USERID_KEY, userId.toString());
  }

  public getToken(): string | null {
    return window.localStorage.getItem(TOKEN_KEY);
  }

  getUserId(): number | null {
    let userId = window.localStorage.getItem(USERID_KEY);
    return userId ? Number(userId) : null;
  }
}
