import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

import { ContactModel } from '../interfaces/contact-model';

@Injectable({
  providedIn: 'root',
})
export class EmailSenderService {
  private emailPATH = 'mail';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };
  constructor(private http: HttpClient) {}

  sendContactMail(contactMail: ContactModel): Observable<ContactModel> {
    return this.http.post<ContactModel>(
      `${environment.apiURL}/${this.emailPATH}/sendContactMail`,
      contactMail,
      this.httpOptions
    );
  }
}
