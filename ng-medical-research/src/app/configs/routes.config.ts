import { InjectionToken } from '@angular/core';

export const ROUTES_CONFIG = new InjectionToken('routes.config');

const basePaths = {
  supply: 'supply',
  clinic: 'clinic',
  medicine: 'medicine',
  research: 'research',
  account: 'account',
  project: 'project',
  auth: 'auth',
};

const routesNames = {
  home: 'home',
  error404: '404',
  error403: '403',
  medicine: {
    medicines: 'medicines',
  },
  supply: {
    supplies: 'supplies',
  },
  clinic: {
    clinics: 'clinics',
  },
  research: {
    patients: 'patients',
    newPatient: 'new-patient',
    detail: ':id',
  },
  account: {
    users: 'users',
    detail: ':id',
  },
  project: {
    projects: 'projects',
    detail: ':id',
  },
  auth: {
    signUp: 'sign-up',
    logIn: 'log-in',
  },
};

export const getResearchDetail = (id: string) => `/${basePaths.research}/${id}`;
export const getAccountDetail = (id: string) => `/${basePaths.account}/${id}`;
export const getProjectDetail = (id: string) => `/${basePaths.project}/${id}`;

export const RoutesConfig: any = {
  basePaths,
  routesNames,
  routes: {
    home: `/${routesNames.home}`,
    error404: `/${routesNames.error404}`,
    error403: `/${routesNames.error403}`,
    medicine: {
      medicines: `/${basePaths.medicine}/${routesNames.medicine.medicines}`,
    },
    supply: {
      supplies: `/${basePaths.supply}/${routesNames.supply.supplies}`,
    },
    clinic: {
      clinics: `/${basePaths.clinic}/${routesNames.clinic.clinics}`,
    },
    research: {
      researches: `/${basePaths.research}/${routesNames.research.patients}`,
      newPatient: `/${basePaths.research}/${routesNames.research.newPatient}`,
      detail: getResearchDetail,
    },
    account: {
      users: `${basePaths.account}/${routesNames.account.users}`,
      detail: getAccountDetail,
    },
    project: {
      projects: `${basePaths.project}/${routesNames.project.projects}`,
      detail: getProjectDetail,
    },
    auth: {
      signUp: `/${basePaths.auth}/${routesNames.auth.signUp}`,
      logIn: `/${basePaths.auth}/${routesNames.auth.logIn}`,
    },
  },
};
