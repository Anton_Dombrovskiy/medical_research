import { Component, OnInit } from '@angular/core';
import { RoutesConfig } from './configs/routes.config';
import { TokenService } from './shared/services/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  constructor(private tokenService: TokenService) {}
  ngOnInit(): void {
    window.addEventListener(
      'storage',
      (event) => {
        if (event.storageArea == localStorage) {
          let token = this.tokenService.getToken();
          if (!token) {
            window.location.href = RoutesConfig.routes.home;
          }
        }
      },
      false
    );
  }
}
