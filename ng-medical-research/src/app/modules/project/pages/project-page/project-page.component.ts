import { Component, OnInit } from '@angular/core';
import { Roles } from '../../../../modules/user/shared/user.model';
import { UserService } from '../../../../modules/user/shared/user.service';
import { ProjectDetail } from '../../shared/project';
import { ProjectService } from '../../shared/project.service';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css'],
})
export class ProjectPageComponent implements OnInit {
  tableHeaders: string[] = [
    'Title',
    'Date of news',
    'Background image name',
    'Detailed information',
  ];

  projects?: ProjectDetail[];
  displayedData?: ProjectDetail[];
  userRole?: Roles;
  modalAction: 'edit' | 'add' = 'add';
  userMessage = 'Loading...';
  constructor(
    private projectService: ProjectService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getProjects();
    this.userService.getRole().subscribe((role) => (this.userRole = role.name));
  }

  getProjects() {
    this.userMessage = 'Loading...';
    this.projectService.getAll().subscribe(
      (p) => {
        this.userMessage = '';
        this.projects = p;
        this.displayedData = this.projects;
      },
      (err: any) =>
        (this.userMessage = `Error: status: ${err.status}, message: ${err.statusText}`)
    );
  }

  addProject(project: ProjectDetail) {
    this.projectService.add(project).subscribe(() => {
      this.getProjects();
      this.showAddModal = false;
    });
  }

  deleteProject(id: string | number) {
    this.projects = this.projects?.filter((p) => p.id != id);
    this.projectService.delete(id).subscribe(() => this.getProjects());
  }

  editableItem: ProjectDetail | null = null;
  openEditModal(project: ProjectDetail) {
    this.modalAction = 'edit';
    this.showAddModal = true;
    this.editableItem = project;
  }

  editProject(project: ProjectDetail) {
    this.projectService.update(project).subscribe(() => {
      this.getProjects();
      this.showAddModal = false;
    });
  }

  search(searchString: string) {
    let search = searchString.toLowerCase().trim();
    if (search.length === 0) {
      this.displayedData = this.projects;
    } else {
      this.displayedData = this.projects?.filter((p) =>
        p.title.toLowerCase().includes(search)
      );
    }
  }

  showAddModal: boolean = false;
  openAddModal() {
    this.modalAction = 'add';
    this.showAddModal = true;
  }

  canAdd() {
    return this.userRole === Roles.Sponsor || this.userRole === Roles.Admin;
  }

  canModify() {
    return this.userRole === Roles.Sponsor || this.userRole === Roles.Admin;
  }

  canDelete() {
    return this.userRole === Roles.Sponsor || this.userRole === Roles.Admin;
  }
}
