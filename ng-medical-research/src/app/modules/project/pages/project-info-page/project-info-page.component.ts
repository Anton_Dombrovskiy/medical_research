import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ProjectDetail } from '../../shared/project';
import { ProjectService } from '../../shared/project.service';

@Component({
  selector: 'app-project-info-page',
  templateUrl: './project-info-page.component.html',
  styleUrls: ['./project-info-page.component.css'],
})
export class ProjectInfoPageComponent implements OnInit {
  projectId: number;
  project?: ProjectDetail;
  projectImgURL?: string;
  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService
  ) {
    this.projectId = Number(this.route.snapshot.paramMap.get('id'));
  }

  getProject() {
    this.projectService.get(this.projectId).subscribe((p) => {
      this.project = p;
      this.projectImgURL = `${environment.projectImgURL}/${this.project.backgroundImageName}`;
    });
  }

  ngOnInit(): void {
    this.getProject();
  }
}
