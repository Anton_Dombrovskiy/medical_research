import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { Project, ProjectDetail } from '../../shared/project';
import { ProjectService } from '../../shared/project.service';

@Component({
  selector: 'app-add-edit-project',
  templateUrl: './add-edit-project.component.html',
  styleUrls: [
    './add-edit-project.component.css',
    '../../../../shared/styles/modal.css',
  ],
})
export class AddEditProjectComponent implements OnInit {
  id?: number;
  title?: string;
  dateOfNews?: Date;
  backgroundImgURL:
    | string
    | ArrayBuffer
    | null = `${environment.projectImgURL}/default.jpg`;
  detailedInfo?: string;
  file: any;
  constructor(private projectService: ProjectService) {}

  addProject(projectForm: NgForm) {
    if (projectForm.valid && confirm('Confirm acntion')) {
      let formData: FormData | null = null;
      if (this.file) {
        formData = new FormData();
        formData.append('uploadedFile', this.file, this.file.name);
      }

      this.projectService.uploadPhoto(formData).subscribe((data: any) => {
        this.addItemEvent.next({
          id: 0,
          title: this.title!,
          dateOfNews: this.dateOfNews!,
          backgroundImageName: data?.fileName ? data.fileName : null,
          detailedInformation: this.detailedInfo!,
        } as ProjectDetail);

        this.closeModal(projectForm);
      });
    }
  }

  editProject(projectForm: NgForm) {
    if (projectForm.valid && confirm('Confirm acntion')) {
      let formData: FormData | null = null;
      if (this.file) {
        formData = new FormData();
        formData.append('uploadedFile', this.file, this.file.name);
      }

      this.projectService.uploadPhoto(formData).subscribe((data: any) => {
        this.editItemEvent.next({
          id: this.id!,
          title: this.title!,
          dateOfNews: this.dateOfNews!,
          backgroundImageName: data?.fileName ? data.fileName : null,
          detailedInformation: this.detailedInfo!,
        } as ProjectDetail);

        this.closeModal(projectForm);
      });
    }
  }

  Close(projectForm: NgForm) {
    if (confirm('Confirm acntion')) {
      this.closeModal(projectForm);
    }
  }

  private closeModal(projectForm: NgForm) {
    projectForm.reset();
    this.showModalChange.emit(false);
  }

  ngOnInit(): void {
    if (this.action === 'edit' && this.item) {
      this.id = this.item.id;
      this.title = this.item.title;
      this.dateOfNews = this.item.dateOfNews;
      this.backgroundImgURL = `${environment.projectImgURL}/${this.item.backgroundImageName}`;
      this.detailedInfo = this.item.detailedInformation;
    }
  }

  readURL(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        this.backgroundImgURL = reader.result;
        this.file = file;
      };

      reader.readAsDataURL(file);
    }
  }

  openFileDialog() {
    document.getElementById('backgroundImg')?.click();
  }

  parseDate(target: any) {
    if (target) {
      return new Date(target.value);
    }

    return new Date();
  }

  @Input() showModal: boolean = false;
  @Input() item: ProjectDetail | null = null;
  @Input() action: 'edit' | 'add' = 'add';
  @Output() showModalChange = new EventEmitter<boolean>();
  @Output() editItemEvent = new EventEmitter<ProjectDetail>();
  @Output() addItemEvent = new EventEmitter<ProjectDetail>();
}
