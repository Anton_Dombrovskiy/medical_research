import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';
import { ProjectInfoPageComponent } from './pages/project-info-page/project-info-page.component';
import { ProjectPageComponent } from './pages/project-page/project-page.component';

const projectRoutes = RoutesConfig.routesNames.project;

const medicinesRoutes: Routes = [
  {
    path: projectRoutes.projects,
    component: ProjectPageComponent,
    //canActivate: [MedicineGuard],
  },
  {
    path: projectRoutes.detail,
    component: ProjectInfoPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(medicinesRoutes)],
  exports: [RouterModule],
  //providers: [MedicineGuard],
})
export class ProjectRoutingModule {}
