import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ProjectPageComponent } from './pages/project-page/project-page.component';
import { ProjectInfoPageComponent } from './pages/project-info-page/project-info-page.component';
import { ProjectRoutingModule } from './project-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AddEditProjectComponent } from './components/add-edit-project/add-edit-project.component';

@NgModule({
  declarations: [ProjectPageComponent, ProjectInfoPageComponent, AddEditProjectComponent],
  imports: [CommonModule, ProjectRoutingModule, SharedModule, FormsModule],
})
export class ProjectModule {}
