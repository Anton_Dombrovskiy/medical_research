export interface Project {
  id: number;
  title: string;
  dateOfNews: Date;
  backgroundImageName: string | null;
}

export interface ProjectDetail {
  id: number;
  title: string;
  dateOfNews: Date;
  backgroundImageName: string | null;
  detailedInformation: string;
}
