import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, tap } from 'rxjs';
import { environment } from '../../../../environments/environment';

import { Project, ProjectDetail } from './project';

@Injectable({
  providedIn: 'root',
})
export class ProjectService {
  private headerOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };
  constructor(private http: HttpClient) {}

  getAllShort() {
    return this.http
      .get<Project[]>(`${environment.apiURL}/researchproject`)
      .pipe(
        tap((proj) =>
          proj.forEach((p) => {
            p.dateOfNews = new Date(p.dateOfNews);
          })
        )
      );
  }

  getAll(): Observable<ProjectDetail[]> {
    return this.http
      .get<ProjectDetail[]>(`${environment.apiURL}/researchproject/detailed`)
      .pipe(
        tap((proj) =>
          proj.forEach((p) => {
            p.dateOfNews = new Date(p.dateOfNews);
          })
        )
      );
  }

  get(id: string | number): Observable<ProjectDetail> {
    return this.http.get<ProjectDetail>(
      `${environment.apiURL}/researchproject/${id}`
    );
  }

  add(project: ProjectDetail) {
    return this.http.post(
      `${environment.apiURL}/researchproject`,
      project,
      this.headerOptions
    );
  }

  update(project: ProjectDetail) {
    return this.http.put(
      `${environment.apiURL}/researchproject/${project.id}`,
      project,
      this.headerOptions
    );
  }

  delete(id: string | number) {
    return this.http.delete(`${environment.apiURL}/researchproject/${id}`);
  }

  uploadPhoto(photo: any) {
    if (!photo) {
      return of({});
    }

    return this.http.post(
      `${environment.apiURL}/researchproject/saveProjectBackground`,
      photo
    );
  }
}
