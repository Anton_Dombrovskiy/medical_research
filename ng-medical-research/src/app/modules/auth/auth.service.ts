import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, tap } from 'rxjs';

import { environment } from '../../../environments/environment';
import { TokenService } from '../../shared/services/token.service';
import { TokenJWT } from '../../shared/interfaces/token';
import { RoutesConfig } from 'src/app/configs/routes.config';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private tokenService: TokenService) {}
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    }),
    withCredentials: true,
  };

  isLoggedIn(): boolean {
    return !!this.tokenService.getToken();
  }

  refreshToken(): Observable<TokenJWT> {
    let userId = this.tokenService.getUserId();
    if (!userId) {
      return of();
    }

    return this.http
      .post<TokenJWT>(
        `${environment.apiURL}/account/refresh-token/${userId}`,
        {},
        this.httpOptions
      )
      .pipe(
        tap((res: any) => {
          if (!res.msg) {
            const { access_token, userId } = res;
            this.tokenService.saveToken(access_token, userId);
          } else if (res.msg === 'Invalid token') {
            this.logout().subscribe(
              () => (window.location.href = RoutesConfig.routes.auth.logIn)
            );
          }
        })
      );
  }

  login(
    email: string,
    password: string,
    remember: boolean
  ): Observable<TokenJWT> {
    return this.http
      .post<TokenJWT>(
        `${environment.apiURL}/account/login`,
        { email, password, remember },
        this.httpOptions
      )
      .pipe(
        tap((res: any) => {
          if (!res.msg) {
            const { access_token, userId } = res;
            this.tokenService.saveToken(access_token, userId);
          }
        })
      );
  }

  register(
    firstName: string,
    lastName: string,
    initials: string,
    email: string,
    password: string
  ): Observable<TokenJWT> {
    return this.http
      .post<TokenJWT>(
        `${environment.apiURL}/account/register`,
        { firstName, lastName, initials, email, password },
        this.httpOptions
      )
      .pipe(
        tap((res: any) => {
          if (!res.msg) {
            const { access_token, userId } = res;
            this.tokenService.saveToken(access_token, userId);
          }
        })
      );
  }

  logout(): Observable<any> {
    return this.http
      .post(`${environment.apiURL}/account/revoke-token`, {}, this.httpOptions)
      .pipe(
        tap(() => {
          this.tokenService.removeToken();
        })
      );
  }
}
