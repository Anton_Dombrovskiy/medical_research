import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutesConfig } from '../../../../configs/routes.config';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-log-in-page',
  templateUrl: './log-in-page.component.html',
  styleUrls: ['./log-in-page.component.css'],
})
export class LogInPageComponent implements OnInit {
  loginError = '';
  email: string = '';
  password: string = '';
  rememberMe: boolean = false;
  constructor(private authService: AuthService, private rout: Router) {}

  login(loginForm: NgForm): void {
    if (loginForm.valid) {
      this.loginError = '';
      this.authService
        .login(this.email, this.password, this.rememberMe)
        .subscribe((res: any) => {
          if (!res.msg) {
            window.location.href = RoutesConfig.routes.home;
          } else {
            const { msg } = res;
            this.loginError = msg;
          }
        });
    }
  }

  ngOnInit(): void {}
}
