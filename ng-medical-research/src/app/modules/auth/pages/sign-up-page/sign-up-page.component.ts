import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { RoutesConfig } from 'src/app/configs/routes.config';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-sign-up-page',
  templateUrl: './sign-up-page.component.html',
  styleUrls: ['./sign-up-page.component.css'],
})
export class SignUpPageComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.signUpForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      initials: this.initials,
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
    });
  }

  signUpForm: FormGroup;
  firstName = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
  ]);
  lastName = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
  ]);
  initials = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [
    Validators.required,
    Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$'),
  ]);
  confirmPassword = new FormControl('', [Validators.required]);

  ngOnInit(): void {}

  register() {
    if (
      this.signUpForm.valid &&
      this.signUpForm.value.password === this.signUpForm.value.confirmPassword
    ) {
      const formValue = this.signUpForm.value;
      this.authService
        .register(
          formValue.firstName,
          formValue.lastName,
          formValue.initials,
          formValue.email,
          formValue.password
        )
        .subscribe((res: any) => {
          if (!res.msg) {
            window.location.href = RoutesConfig.routes.home;
          } else {
            alert(res.msg);
          }
        });
    }
  }

  getErrorMessage(field: any): string | void {
    // @ts-ignore
    const classField: any = this[field];
    if (classField?.hasError('required')) {
      return '*This field is required';
    } else if (classField?.hasError('email')) {
      return '*Not a valid email';
    } else if (classField?.hasError('pattern')) {
      return '*Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters.';
    }
  }
}
