import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { Error404PageComponent } from './pages/error404-page/error404-page.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ContactBlockComponent } from './shared/contact-block/contact-block.component';
import { CurrentTopicsComponent } from './shared/current-topics/current-topics.component';
import { TaglineComponent } from './shared/tagline/tagline.component';
import { MapSectionComponent } from './shared/map-section/map-section.component';
import { FormsModule } from '@angular/forms';
import { RootRoutingModule } from './root-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { Error403PageComponent } from './pages/error403-page/error403-page.component';

@NgModule({
  declarations: [
    HomePageComponent,
    Error404PageComponent,
    HeaderComponent,
    FooterComponent,
    ContactBlockComponent,
    CurrentTopicsComponent,
    TaglineComponent,
    MapSectionComponent,
    Error403PageComponent,
  ],
  imports: [CommonModule, FormsModule, SharedModule, RootRoutingModule],
  exports: [
    HomePageComponent,
    Error404PageComponent,
    HeaderComponent,
    FooterComponent,
  ],
})
export class RootModule {}
