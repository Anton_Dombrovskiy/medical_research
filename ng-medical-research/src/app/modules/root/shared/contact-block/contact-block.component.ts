import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ContactModel } from '../../../../shared/interfaces/contact-model';
import { EmailSenderService } from '../../../../shared/services/email-sender.service';

@Component({
  selector: 'app-contact-block',
  templateUrl: './contact-block.component.html',
  styleUrls: ['./contact-block.component.css'],
})
export class ContactBlockComponent implements OnInit {
  submitButtonDisabled = false;
  constructor(private emailSenderService: EmailSenderService) {}
  contactInfo: ContactModel = {
    email: '',
    message: '',
  };
  sendData(contactForm: NgForm): void {
    if (contactForm.valid) {
      if (this.contactInfo.message.trim().length == 0) {
        alert("Message can't be a space");
        return;
      }

      this.submitButtonDisabled = true;
      this.emailSenderService
        .sendContactMail(this.contactInfo)
        .subscribe((res: any) => {
          if (res?.msg) {
            alert(res.msg);
          } else {
            contactForm.reset();
          }

          this.submitButtonDisabled = false;
        });
    }
  }
  ngOnInit(): void {}
}
