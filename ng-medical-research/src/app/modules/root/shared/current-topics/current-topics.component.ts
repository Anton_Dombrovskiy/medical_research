import { Component, OnDestroy, OnInit } from '@angular/core';

import { ProjectService } from '../../../project/shared/project.service';
import { Project } from '../../../project/shared/project';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-current-topics',
  templateUrl: './current-topics.component.html',
  styleUrls: ['./current-topics.component.css'],
})
export class CurrentTopicsComponent implements OnInit, OnDestroy {
  constructor(private mProjectService: ProjectService) {}
  private currentProjects: Project[] = [];
  private currentSliderId: number = 0;
  private sliderIntervalId?: number;
  currentSlides: Project[] = [];
  getProjects(): void {
    this.mProjectService.getAllShort().subscribe((projects) => {
      this.currentProjects = projects;
      this.startSlider();
    });
  }

  startSlider(): void {
    if (this.currentProjects.length > 0) {
      this.chageSlides();
      this.sliderIntervalId = setInterval(() => {
        this.chageSlides();
      }, 5000);
    }
  }

  chageSlides(): void {
    for (let i = 0; i < this.currentProjects.length && i < 2; i++) {
      if (this.currentSliderId >= this.currentProjects.length) {
        this.currentSliderId = 0;
      }

      this.currentSlides[i] = this.currentProjects[this.currentSliderId++];
    }
  }

  getImageURL(project: Project) {
    return `${environment.projectImgURL}/${project.backgroundImageName}`;
  }

  ngOnInit(): void {
    this.getProjects();
  }

  ngOnDestroy(): void {
    clearInterval(this.sliderIntervalId);
  }
}
