import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../../user/shared/user.service';
import { AuthService } from '../../../auth/auth.service';
import { Roles, ShortUserInfo } from '../../../user/shared/user.model';
import { TokenService } from '../../../../shared/services/token.service';
import { ROUTES_CONFIG } from '../../../../configs/routes.config';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [
    './header.component.css',
    './styles/navigation.css',
    './styles/user-logo-container.css',
  ],
})
export class HeaderComponent implements OnInit {
  showMoreInfo: boolean = false;
  currentUser: ShortUserInfo | null = null;
  isLoggedIn: boolean = false;
  showBurgerMenu: boolean = false;
  userAvatarURL?: string;
  defaultAvatarURL = `${environment.userAvatarURL}/default.png`;
  constructor(
    @Inject(ROUTES_CONFIG) public routesConfig: any,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private tokenService: TokenService
  ) {}

  logout() {
    this.showMoreInfo = false;
    this.authService.logout().subscribe(() => {
      this.isLoggedIn = false;
      window.location.href = this.routesConfig.routes.auth.logIn;
    });
  }

  isAdmin() {
    return this.currentUser?.role === Roles.Admin;
  }

  isSponsor() {
    return this.currentUser?.role === Roles.Sponsor;
  }

  isManager() {
    return this.currentUser?.role === Roles.Manager;
  }

  isResearcher() {
    return this.currentUser?.role === Roles.Researcher;
  }

  redirectToAccount() {
    this.router.navigate([
      this.routesConfig.routes.account.detail(this.currentUser?.id),
    ]);
    this.showMoreInfo = false;
  }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn();
    if (this.isLoggedIn)
      this.userService
        .getShortUserInfo(+this.tokenService.getUserId()!)
        .subscribe((u) => {
          this.currentUser = u;
          this.userAvatarURL = `${environment.userAvatarURL}/${this.currentUser.userAvatarName}`;
        });
  }
}
