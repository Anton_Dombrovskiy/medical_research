import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClinicPageComponent } from './pages/clinic-page/clinic-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ClinicRoutingModule } from './clinic-routing.module';
import { FormsModule } from '@angular/forms';
import { AddEditClinicComponent } from './components/add-edit-clinic/add-edit-clinic.component';

@NgModule({
  declarations: [ClinicPageComponent, AddEditClinicComponent],
  imports: [CommonModule, SharedModule, ClinicRoutingModule, FormsModule],
})
export class ClinicModule {}
