export interface Clinic {
  id: number;
  name: string;
  city: string;
  primaryAddress: string;
  secondaryAddress: string;
  phoneNumber: string;
}
