import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Clinic } from './clinic';

@Injectable({
  providedIn: 'root',
})
export class ClinicService {
  private headerOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<Clinic[]>(
      `${environment.apiURL}/clinic`,
      this.headerOptions
    );
  }

  add(clinic: Clinic) {
    return this.http.post<Clinic>(
      `${environment.apiURL}/clinic`,
      clinic,
      this.headerOptions
    );
  }

  update(clinic: Clinic) {
    return this.http.put<Clinic>(
      `${environment.apiURL}/clinic/${clinic.id}`,
      clinic,
      this.headerOptions
    );
  }
}
