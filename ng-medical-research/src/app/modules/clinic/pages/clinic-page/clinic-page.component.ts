import { Component, OnInit } from '@angular/core';
import { ClinicService } from '../../shared/clinic.service';
import { Clinic } from '../../shared/clinic';

@Component({
  selector: 'app-clinic-page',
  templateUrl: './clinic-page.component.html',
  styleUrls: ['./clinic-page.component.css'],
})
export class ClinicPageComponent implements OnInit {
  tableHeaders: string[] = [
    'Id',
    'Name',
    'City',
    'Address',
    'Address 2',
    'Phone',
  ];

  clinics?: Clinic[];
  displayedData?: Clinic[];
  modalAction: 'edit' | 'add' = 'add';
  userMessage = 'Loading...';
  constructor(private clinicService: ClinicService) {}

  ngOnInit(): void {
    this.getClinic();
  }

  getClinic() {
    this.userMessage = 'Loading...';
    this.clinicService.getAll().subscribe(
      (c) => {
        this.userMessage = '';
        this.clinics = c;
        this.displayedData = this.clinics;
      },
      (err: any) =>
        (this.userMessage = `Error: status: ${err.status}, message: ${err.statusText}`)
    );
  }

  addClinic(clinic: Clinic) {
    this.clinicService.add(clinic).subscribe(() => {
      this.getClinic();
      this.showModal = false;
    });
  }

  editableItem: Clinic | null = null;
  openEditModal(clinic: Clinic) {
    this.modalAction = 'edit';
    this.showModal = true;
    this.editableItem = clinic;
  }

  editClinic(clinic: Clinic) {
    this.clinicService.update(clinic).subscribe(() => {
      this.getClinic();
      this.showModal = false;
    });
  }

  search(searchString: string) {
    let search = searchString.toLowerCase().trim();
    if (search.length === 0) {
      this.displayedData = this.clinics;
    } else {
      this.displayedData = this.clinics?.filter((c) =>
        c.name.toLowerCase().includes(search)
      );
    }
  }

  showModal: boolean = false;
  openAddModal() {
    this.modalAction = 'add';
    this.showModal = true;
  }
}
