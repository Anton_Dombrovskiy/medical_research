import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Clinic } from '../../shared/clinic';

@Component({
  selector: 'app-add-edit-clinic',
  templateUrl: './add-edit-clinic.component.html',
  styleUrls: [
    './add-edit-clinic.component.css',
    '../../../../shared/styles/modal.css',
  ],
})
export class AddEditClinicComponent implements OnInit {
  name?: string;
  city?: string;
  phoneNumber?: string;
  primaryAddress?: string;
  secondaryAddress?: string;
  constructor() {}

  ngOnInit(): void {
    if (this.item && this.action === 'edit') {
      this.name = this.item.name;
      this.city = this.item.city;
      this.phoneNumber = this.item.phoneNumber;
      this.primaryAddress = this.item.primaryAddress;
      this.secondaryAddress = this.item.secondaryAddress;
    }
  }
  addClinic(clinicForm: NgForm) {
    if (clinicForm.valid && confirm('Confirm acntion')) {
      this.addItemEvent.next({
        name: this.name,
        city: this.city,
        phoneNumber: this.phoneNumber,
        primaryAddress: this.primaryAddress,
        secondaryAddress: this.secondaryAddress,
      } as Clinic);

      this.closeModal(clinicForm);
    }
  }
  editClinic(clinicForm: NgForm) {
    if (clinicForm.valid && confirm('Confirm acntion')) {
      this.editItemEvent.next({
        id: this.item?.id,
        name: this.name,
        city: this.city,
        phoneNumber: this.phoneNumber,
        primaryAddress: this.primaryAddress,
        secondaryAddress: this.secondaryAddress,
      } as Clinic);

      this.closeModal(clinicForm);
    }
  }

  Close(clinicForm: NgForm) {
    if (confirm('Confirm acntion')) {
      this.closeModal(clinicForm);
    }
  }

  closeModal(clinicForm: NgForm) {
    clinicForm.reset();
    this.showModalChange.emit(false);
  }

  @Input() action: 'edit' | 'add' = 'add';
  @Input() item: Clinic | null = null;
  @Input() showModal: boolean = false;
  @Output() showModalChange = new EventEmitter<boolean>();
  @Output() editItemEvent = new EventEmitter<Clinic>();
  @Output() addItemEvent = new EventEmitter<Clinic>();
}
