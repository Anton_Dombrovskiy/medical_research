import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';
import { ClinicGuard } from './clinic.guard';
import { ClinicPageComponent } from './pages/clinic-page/clinic-page.component';

const clinicRoutes = RoutesConfig.routesNames.clinic;

const clinicsRoutes: Routes = [
  {
    path: clinicRoutes.clinics,
    component: ClinicPageComponent,
    canActivate: [ClinicGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(clinicsRoutes)],
  exports: [RouterModule],
  providers: [ClinicGuard],
})
export class ClinicRoutingModule {}
