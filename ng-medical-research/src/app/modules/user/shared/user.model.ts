export interface User {
  id: number;
  firstName: string;
  lastName: string;
  initials: string;
  email: string;
  password: string;
  userAvatarName: string;
}

export interface ShortUserInfo {
  id: number;
  firstName: string;
  lastName: string;
  userAvatarName: string;
  role: Roles;
}

export interface UpdateUser {
  id: number;
  firstName: number;
  lastName: string;
  initials: string;
  newPassword: string;
  userAvatarName: string | null;
}

export interface UpdateSensitiveUser {
  id: number;
  email: string;
  role: string;
  attachedClinic: string;
  isLocked: boolean;
}

export interface Role {
  id: number;
  name: Roles;
}

export enum Roles {
  Admin = 'Admin',
  Sponsor = 'Sponsor',
  Researcher = 'Researcher',
  Manager = 'Manager',
}
