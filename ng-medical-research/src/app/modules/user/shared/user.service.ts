import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import { TokenService } from '../../../shared/services/token.service';

import { environment } from '../../../../environments/environment';
import {
  User,
  ShortUserInfo,
  UpdateUser,
  Role,
  UpdateSensitiveUser,
} from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private headerOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };

  constructor(private http: HttpClient, private tokenService: TokenService) {}

  getAllSensitive() {
    return this.http.get<UpdateSensitiveUser[]>(
      `${environment.apiURL}/account/sensitive`,
      this.headerOptions
    );
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${environment.apiURL}/account/${id}`);
  }

  getShortUserInfo(id: number): Observable<ShortUserInfo> {
    return this.http.get<ShortUserInfo>(
      `${environment.apiURL}/account/user-info/${id}`
    );
  }

  update(user: UpdateUser) {
    return this.http.put(
      `${environment.apiURL}/account/${user.id}`,
      user,
      this.headerOptions
    );
  }

  updateSensitive(user: UpdateSensitiveUser) {
    return this.http.put(
      `${environment.apiURL}/account/update-sensitive/${user.id}`,
      user,
      this.headerOptions
    );
  }

  delete(id: number) {
    return this.http.delete(
      `${environment.apiURL}/account/${id}`,
      this.headerOptions
    );
  }

  getRole() {
    return this.http.get<Role>(
      `${
        environment.apiURL
      }/account/user-role/${this.tokenService.getUserId()}`,
      this.headerOptions
    );
  }

  getAttachedClinic() {
    return this.http.get<string>(
      `${
        environment.apiURL
      }/account/attached-clinic/${this.tokenService.getUserId()}`
    );
  }

  uploadPhoto(photo: any) {
    if (!photo) {
      return of({});
    }

    return this.http.post(`${environment.apiURL}/account/setAvatar`, photo);
  }
}
