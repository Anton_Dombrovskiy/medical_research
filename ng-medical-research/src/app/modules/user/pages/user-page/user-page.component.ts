import { Component, OnInit } from '@angular/core';
import { UpdateSensitiveUser } from '../../shared/user.model';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css'],
})
export class UserPageComponent implements OnInit {
  tableHeaders: string[] = ['Id', 'Email', 'Role', 'Clinic Id', 'Locked'];

  users?: UpdateSensitiveUser[];
  displayedData?: UpdateSensitiveUser[];
  userMessage = 'Loading...';
  showModal = false;
  constructor(private userService: UserService) {}

  getUsers() {
    this.userMessage = 'Loading...';
    this.userService.getAllSensitive().subscribe(
      (u) => {
        this.userMessage = '';
        this.users = u;
        this.displayedData = this.users;
      },
      (err: any) =>
        (this.userMessage = `Error: status: ${err.status}, message: ${err.statusText}`)
    );
  }

  searchByEmail(searchString: string) {
    let search = searchString.toLowerCase().trim();
    if (search.length === 0) {
      this.displayedData = this.users;
    } else {
      this.displayedData = this.users?.filter((user) =>
        user.email.toLowerCase().includes(search)
      );
    }
  }

  editableItem: UpdateSensitiveUser | null = null;
  openEditModal(medicine: UpdateSensitiveUser) {
    this.showModal = true;
    this.editableItem = medicine;
  }

  editMedicine(user: UpdateSensitiveUser) {
    this.userService.updateSensitive(user).subscribe(() => {
      this.getUsers();
      this.showModal = false;
    });
  }

  ngOnInit(): void {
    this.getUsers();
  }
}
