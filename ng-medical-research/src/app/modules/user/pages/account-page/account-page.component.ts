import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RoutesConfig } from '../../../../configs/routes.config';
import { AuthService } from '../../../../modules/auth/auth.service';
import { environment } from '../../../../../environments/environment';
import { UpdateUser } from '../../shared/user.model';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.css'],
})
export class AccountPageComponent implements OnInit {
  accountForm: FormGroup;
  firstName = new FormControl('', [Validators.required, Validators.min(2)]);

  lastName = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
  ]);

  initials = new FormControl('', [Validators.required]);

  email = new FormControl('', [Validators.required, Validators.email]);
  oldPassword = new FormControl('', [Validators.required]);

  newPassword = new FormControl('', [
    Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$'),
  ]);

  confirmPassword = new FormControl('', []);

  userId: number;
  userIcoUrl: string | ArrayBuffer | null = null;
  userName?: string;
  photoFileName?: string;
  showExtraOptions: boolean = false;
  file: any;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.userId = Number(this.route.snapshot.paramMap.get('id'));
    this.oldPassword.disable();
    this.email.disable();
    this.accountForm = this.formBuilder.group({
      firstName: this.firstName,
      lastName: this.lastName,
      initials: this.initials,
      email: this.email,
      oldPassword: this.oldPassword,
      newPassword: this.newPassword,
      confirmPassword: this.confirmPassword,
    });
  }

  getUser() {
    this.userService.getUser(this.userId).subscribe((res: any) => {
      if (!res.errors) {
        this.firstName.setValue(res.firstName);
        this.lastName.setValue(res.lastName);
        this.initials.setValue(res.initials);
        this.email.setValue(res.email);
        this.oldPassword.setValue('1111111111111');
        this.userIcoUrl = `${environment.userAvatarURL}/${res.userAvatarName}`;
        this.userName = `${res.firstName} ${res.lastName}`;
      }
    });
  }

  updateInfo() {
    if (
      this.accountForm.valid &&
      this.accountForm.value.newPassword ===
        this.accountForm.value.confirmPassword &&
      confirm('Confirm acntion')
    ) {
      const formValue = this.accountForm.value;
      let formData: FormData | null = null;
      if (this.file) {
        formData = new FormData();
        formData.append('uploadedFile', this.file, this.file.name);
      }

      this.userService.uploadPhoto(formData).subscribe((data: any) => {
        this.userService
          .update({
            id: this.userId,
            firstName: formValue.firstName,
            lastName: formValue.lastName,
            initials: formValue.initials,
            newPassword: formValue.newPassword,
            userAvatarName: data?.fileName ? data.fileName : null,
          } as UpdateUser)
          .subscribe(() => {
            this.getUser();
            window.location.reload();
          });
      });
    }
  }

  openFileDialog() {
    document.getElementById('userPhoto')?.click();
  }

  readURL(event: any) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        this.userIcoUrl = reader.result;
        this.file = file;
      };

      reader.readAsDataURL(file);
    }
  }

  removeAccount() {
    if (confirm('Confirm acntion')) {
      this.userService.delete(this.userId).subscribe(() => {
        this.showExtraOptions = false;
        this.authService
          .logout()
          .subscribe(
            () => (window.location.href = RoutesConfig.routes.auth.logIn)
          );
      });
    }
  }

  ngOnInit(): void {
    this.getUser();
  }
}
