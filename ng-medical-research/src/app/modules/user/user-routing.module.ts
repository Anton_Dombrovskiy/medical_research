import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';
import { AccountPageComponent } from './pages/account-page/account-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { UserGuard } from './user.guard';

const userRoutes = RoutesConfig.routesNames.account;

const usersRoutes: Routes = [
  { path: userRoutes.users, component: UserPageComponent },
  {
    path: userRoutes.detail,
    component: AccountPageComponent,
    canActivate: [UserGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(usersRoutes)],
  exports: [RouterModule],
  providers: [UserGuard],
})
export class UserRoutingModule {}
