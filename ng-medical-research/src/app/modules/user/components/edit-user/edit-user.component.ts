import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ClinicName } from '../../../../modules/supply/shared/supply';
import { Roles, UpdateSensitiveUser } from '../../shared/user.model';
import { SupplyService } from 'src/app/modules/supply/shared/supply.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: [
    './edit-user.component.css',
    '../../../../shared/styles/modal.css',
  ],
})
export class EditUserComponent implements OnInit {
  roles: string[] = [];
  clinics?: ClinicName[];
  id?: number;
  email?: string;
  role?: string;
  clinicId?: string;
  isLocked?: boolean;
  constructor(private supplyService: SupplyService) {
    this.roles = Object.values(Roles);
  }

  getClinics() {
    this.supplyService
      .getClinicNames()
      .subscribe((clinicNames) => (this.clinics = clinicNames));
  }

  editMedicine(userForm: NgForm) {
    if (userForm.valid && confirm('Confirm acntion')) {
      this.editEvent.next({
        id: this.id!,
        email: this.email!,
        role: this.role!,
        attachedClinic: this.clinicId!,
        isLocked: this.isLocked!,
      });

      this.closeModal(userForm);
    }
  }

  Close(userForm: NgForm) {
    if (confirm('Confirm acntion')) {
      this.closeModal(userForm);
    }
  }

  closeModal(userForm: NgForm) {
    userForm.reset();
    this.showModalChange.emit(false);
  }

  ngOnInit(): void {
    if (this.item) {
      this.id = this.item.id;
      this.email = this.item.email;
      this.role = this.item.role;
      this.isLocked = this.item.isLocked;
      this.clinicId = this.item.attachedClinic;
    }

    this.getClinics();
  }

  @Input() showModal: boolean = false;
  @Input() item: UpdateSensitiveUser | null = null;
  @Output() showModalChange = new EventEmitter<boolean>();
  @Output() editEvent = new EventEmitter<UpdateSensitiveUser>();
}
