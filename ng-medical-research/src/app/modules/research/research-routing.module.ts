import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';
import { NewPatientPageComponent } from './pages/new-patient-page/new-patient-page.component';
import { PatientInfoComponent } from './pages/patient-info/patient-info.component';
import { ResearchPageComponent } from './pages/research-page/research-page.component';
import { ResearchGuard } from './research.guard';

const researchRoutes = RoutesConfig.routesNames.research;

const researchesRoutes: Routes = [
  {
    path: researchRoutes.newPatient,
    component: NewPatientPageComponent,
    canActivate: [ResearchGuard],
  },
  {
    path: researchRoutes.patients,
    component: ResearchPageComponent,
    canActivate: [ResearchGuard],
  },
  {
    path: researchRoutes.detail,
    component: PatientInfoComponent,
    canActivate: [ResearchGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(researchesRoutes)],
  exports: [RouterModule],
  providers: [ResearchGuard],
})
export class ResearchRoutingModule {}
