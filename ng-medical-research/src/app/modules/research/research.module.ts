import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ResearchPageComponent } from './pages/research-page/research-page.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ResearchRoutingModule } from './research-routing.module';
import { NewPatientPageComponent } from './pages/new-patient-page/new-patient-page.component';
import { PatientInfoComponent } from './pages/patient-info/patient-info.component';

@NgModule({
  declarations: [ResearchPageComponent, NewPatientPageComponent, PatientInfoComponent],
  imports: [CommonModule, SharedModule, ResearchRoutingModule, FormsModule],
})
export class ResearchModule {}
