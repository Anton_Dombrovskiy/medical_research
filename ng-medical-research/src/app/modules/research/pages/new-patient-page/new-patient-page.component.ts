import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PatientPost, Sex } from '../../shared/patient';
import { ResearchService } from '../../shared/research.service';

@Component({
  selector: 'app-new-patient-page',
  templateUrl: './new-patient-page.component.html',
  styleUrls: ['./new-patient-page.component.css'],
})
export class NewPatientPageComponent implements OnInit {
  patientNumber?: string;
  dateOfBirthday?: Date;
  sex?: string = '0';
  userMessage = '';
  constructor(private researchService: ResearchService) {}

  ngOnInit(): void {}

  addPatient(newPatientForm: NgForm) {
    this.userMessage = '';
    if (newPatientForm.valid) {
      this.researchService
        .add({
          patientNumber: this.patientNumber,
          dateOfBirthday: this.dateOfBirthday,
          sex: +this.sex!,
        } as PatientPost)
        .subscribe(
          () => {
            this.userMessage = 'New patient was added';
            newPatientForm.reset();
          },
          (err: any) => {
            this.userMessage = err.error.msg;
          }
        );
    }
  }
}
