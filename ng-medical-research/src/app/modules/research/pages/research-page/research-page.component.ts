import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Roles } from 'src/app/modules/user/shared/user.model';
import { UserService } from 'src/app/modules/user/shared/user.service';
import { ROUTES_CONFIG } from '../../../../configs/routes.config';
import { Research } from '../../shared/patient';
import { ResearchService } from '../../shared/research.service';

@Component({
  selector: 'app-research-page',
  templateUrl: './research-page.component.html',
  styleUrls: ['./research-page.component.css'],
})
export class ResearchPageComponent implements OnInit {
  tableHeaders = ['Number', 'Date of birthday', 'Last visit date', 'Medicines'];

  patients?: Research[];
  displayedData?: Research[];
  userRole?: Roles;
  hasClinic: boolean = false;
  userMessage = 'Loading...';
  constructor(
    @Inject(ROUTES_CONFIG) public routesConfig: any,
    private researchService: ResearchService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userService.getRole().subscribe((role) => {
      this.userRole = role.name;
      if (this.userRole !== Roles.Researcher) {
        this.getResearchs();
      } else {
        this.userService.getAttachedClinic().subscribe((clinicId) => {
          if (clinicId?.toString().length === 3 && Number(clinicId)) {
            this.hasClinic = true;
            this.getResearchs();
          }
        });
      }
    });
  }

  getResearchs() {
    this.userMessage = 'Loading...';
    this.researchService.getAll().subscribe(
      (m) => {
        this.userMessage = '';
        this.patients = m;
        this.displayedData = this.patients;
      },
      (err: any) =>
        (this.userMessage = `Error: status: ${err.status}, message: ${err.statusText}`)
    );
  }

  search(searchString: string) {
    let search = searchString.toLowerCase().trim();
    if (search.length === 0) {
      this.displayedData = this.patients;
    } else {
      this.displayedData = this.patients?.filter((c) =>
        c.patientNumber.toLowerCase().includes(search)
      );
    }
  }

  redirectToPatientInfoPage(id: string | number) {
    this.router.navigate([this.routesConfig.routes.research.detail(+id)]);
  }

  canAdd() {
    return this.userRole === Roles.Researcher || this.userRole === Roles.Admin;
  }

  canOpenMoreInfo() {
    return this.userRole === Roles.Researcher || this.userRole === Roles.Admin;
  }

  isResearcher() {
    return this.userRole === Roles.Researcher;
  }

  isSponsor() {
    return this.userRole === Roles.Sponsor;
  }

  isAdmin() {
    return this.userRole === Roles.Admin;
  }
}
