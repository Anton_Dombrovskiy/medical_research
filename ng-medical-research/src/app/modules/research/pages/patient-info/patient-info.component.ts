import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Patient, Sex, Status, Visit } from '../../shared/patient';
import { ResearchService } from '../../shared/research.service';

@Component({
  selector: 'app-patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.css'],
})
export class PatientInfoComponent implements OnInit {
  tableHeaders = ['Date', 'Medicine'];
  patientId: number;
  status = Status;
  sex = Sex;
  patient?: Patient;
  visits?: Visit[];
  isPartisipant: boolean = false;
  constructor(
    private researchService: ResearchService,
    private route: ActivatedRoute
  ) {
    this.patientId = Number(this.route.snapshot.paramMap.get('id'));
  }

  getPatient() {
    this.researchService.get(this.patientId).subscribe((p: Patient) => {
      this.patient = p;
      this.isPartisipant =
        p.status !== Status.Finished && p.status !== Status.FinishedEarly;
    });
  }

  getVisits() {
    this.researchService.getVisits(this.patientId).subscribe((v) => {
      this.visits = v;
    });
  }

  performVisit() {
    if (confirm('Confirm acntion')) {
      this.researchService
        .performVisit(this.patientId)
        .subscribe((res: any) => {
          this.getPatient();
          this.getVisits();
        }, this.handleError);
    }
  }

  endPartisipation() {
    if (confirm('Confirm acntion')) {
      this.researchService.delete(this.patientId).subscribe(() => {
        this.getPatient();
        this.getVisits();
      });
    }
  }

  ngOnInit(): void {
    this.getPatient();
    this.getVisits();
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.status !== 0 && errorResponse.error?.msg) {
      alert(errorResponse.error.msg);
    }
  }
}
