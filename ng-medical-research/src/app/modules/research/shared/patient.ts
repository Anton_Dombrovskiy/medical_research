import { MedicineName } from '../../medicine/shared/medicine';

export interface Research {
  id: number;
  patientNumber: string;
  dateOfBirthday: Date;
  lastVisitDate: Date;
  dispensedMedicines: MedicineName[] | string;
}

export interface PatientPost {
  patientNumber: string;
  dateOfBirthday: Date;
  sex: Sex;
}

export interface Patient {
  id: number;
  patientNumber: string;
  dateOfBirthday: Date;
  sex: Sex;
  status: Status;
  medicineType: string;
  lastVisitDate: Date;
}

export interface Visit {
  id: number;
  dateOfVisit: Date;
  dispencedMedicine: string;
}

export enum Sex {
  Male,
  Female,
  Other,
}

export enum Status {
  Screened,
  Randomized,
  Finished,
  FinishedEarly,
}
