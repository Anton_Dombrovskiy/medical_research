import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Patient, PatientPost, Research, Visit } from './patient';

@Injectable({
  providedIn: 'root',
})
export class ResearchService {
  private headerOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http
      .get<Research[]>(`${environment.apiURL}/patient`, this.headerOptions)
      .pipe(
        tap((res: any) => {
          if (!res.errors) {
            res.forEach((r: any) => {
              r.dateOfBirthday = new Date(r.dateOfBirthday);
              r.lastVisitDate = new Date(r.lastVisitDate);
              let medicines: string = '';
              for (const medicine of r.dispensedMedicines) {
                medicines += `${medicine.id}. ${medicine.description}; `;
              }

              r.dispensedMedicines = medicines;
            });
          }
        })
      );
  }

  get(id: number) {
    return this.http
      .get<Patient>(`${environment.apiURL}/patient/${id}`, this.headerOptions)
      .pipe(
        tap((res: any) => {
          if (!res.errors) {
            res.dateOfBirthday = new Date(res.dateOfBirthday);
            res.lastVisitDate = new Date(res.lastVisitDate);
          }
        })
      );
  }

  getVisits(patientId: number) {
    return this.http
      .get<Visit[]>(
        `${environment.apiURL}/patient/GetPatientVisits/${patientId}`,
        this.headerOptions
      )
      .pipe(
        tap((res: any) => {
          if (!res.errors) {
            res.forEach((v: Visit, index: number) => {
              v.dateOfVisit = new Date(v.dateOfVisit);
            });
          }
        })
      );
  }

  performVisit(id: number) {
    return this.http.post(
      `${environment.apiURL}/patient/PerformVisit/${id}`,
      {},
      this.headerOptions
    );
  }

  delete(id: number) {
    return this.http.delete(
      `${environment.apiURL}/patient/${id}`,
      this.headerOptions
    );
  }

  add(patient: PatientPost) {
    return this.http.post<PatientPost>(
      `${environment.apiURL}/patient`,
      patient,
      this.headerOptions
    );
  }

  endParticipation(id: number) {
    this.http.delete(`${environment.apiURL}/patient/${id}`);
  }
}
