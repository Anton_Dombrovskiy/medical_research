import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';
import { AuthService } from '../auth/auth.service';
import { Roles } from '../user/shared/user.model';
import { UserService } from '../user/shared/user.service';
import { PatientInfoComponent } from './pages/patient-info/patient-info.component';

@Injectable({
  providedIn: 'root',
})
export class ResearchGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    return new Promise((resolve) => {
      if (this.authService.isLoggedIn()) {
        this.userService.getRole().subscribe((r) => {
          if (r.name === Roles.Admin || r.name === Roles.Researcher) {
            resolve(true);
          } else if (
            r.name === Roles.Sponsor &&
            route.component !== PatientInfoComponent
          ) {
            resolve(true);
          } else {
            this.router.navigate([RoutesConfig.routes.home]);
            resolve(false);
          }
        });
      } else {
        this.router.navigate([RoutesConfig.routes.home]);
        resolve(false);
      }
    });
  }
}
