import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedicinePageComponent } from './pages/medicine-page/medicine-page.component';
import { MedicineRoutingModule } from './medicine-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { AddEditMedicineComponent } from './components/add-edit-medicine/add-edit-medicine.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MedicinePageComponent, AddEditMedicineComponent],
  imports: [CommonModule, SharedModule, MedicineRoutingModule, FormsModule],
})
export class MedicineModule {}
