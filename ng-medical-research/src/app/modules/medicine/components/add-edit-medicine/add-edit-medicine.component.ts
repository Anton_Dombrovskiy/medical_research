import { Component, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { EventEmitter } from '@angular/core';
import { Medicine, MedicineState } from '../../shared/medicine';

@Component({
  selector: 'app-add-edit-medicine',
  templateUrl: './add-edit-medicine.component.html',
  styleUrls: [
    './add-edit-medicine.component.css',
    './add-edit-medicine-grid.css',
    '../../../../shared/styles/modal.css',
  ],
})
export class AddEditMedicineComponent implements OnInit {
  availableTypes = ['A', 'B', 'C', 'D', 'E', 'F'];
  dosageForms = ['Capsule', 'Tablet'];
  containers = ['Bottle', 'Blister'];
  states = ['Ok', 'Damaged', 'Lost'];

  medicine: Medicine = {
    id: 1,
    description: '',
    type: '',
    dosageForm: '',
    container: '',
    state: MedicineState.Ok,
    expireAt: new Date(),
  };

  minDate = new Date();
  constructor() {}

  addMedicine(medicineForm: NgForm) {
    if (medicineForm.valid && confirm('Confirm acntion')) {
      this.addItemEvent.next({
        id: this.medicine.id,
        description: this.medicine.description,
        type: this.medicine.type,
        dosageForm: this.medicine.dosageForm,
        container: this.medicine.container,
        state: +this.medicine.state,
        expireAt: this.medicine.expireAt,
      });

      this.closeModal(medicineForm);
    }
  }

  editMedicine(medicineForm: NgForm) {
    if (medicineForm.valid && confirm('Confirm acntion')) {
      this.editItemEvent.next({
        id: this.medicine.id,
        description: this.medicine.description,
        type: this.medicine.type,
        dosageForm: this.medicine.dosageForm,
        container: this.medicine.container,
        state: +this.medicine.state,
        expireAt: this.medicine.expireAt,
      } as Medicine);

      this.closeModal(medicineForm);
    }
  }

  Close(medicineForm: NgForm) {
    if (confirm('Confirm acntion')) {
      this.closeModal(medicineForm);
    }
  }

  private closeModal(medicineForm: NgForm) {
    medicineForm.reset();
    this.showModalChange.emit(false);
  }

  ngOnInit(): void {
    if (this.action === 'edit' && this.item) {
      this.medicine = this.item;
    }
  }

  parseDate(target: any) {
    if (target) {
      return new Date(target.value);
    }

    return new Date();
  }

  @Input() showModal: boolean = false;
  @Input() item: Medicine | null = null;
  @Input() action: 'edit' | 'add' = 'add';
  @Output() showModalChange = new EventEmitter<boolean>();
  @Output() editItemEvent = new EventEmitter<Medicine>();
  @Output() addItemEvent = new EventEmitter<Medicine>();
}
