import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Role, Roles } from 'src/app/modules/user/shared/user.model';
import { UserService } from '../../../../modules/user/shared/user.service';
import { Medicine } from '../../shared/medicine';
import { MedicineService } from '../../shared/medicine.service';

@Component({
  selector: 'app-medicine-page',
  templateUrl: './medicine-page.component.html',
  styleUrls: ['./medicine-page.component.css'],
})
export class MedicinePageComponent implements OnInit {
  tableHeaders: string[] = [
    'Id',
    'Description',
    'Type',
    'Dosage form',
    'Container',
    'Medicine state',
    'Expire at',
  ];

  medicines?: Medicine[];
  displayedData?: Medicine[];
  userRole?: Roles;
  modalAction: 'edit' | 'add' = 'add';
  userMessage = 'Loading...';
  constructor(
    private medicineService: MedicineService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.getMedicines();
    this.userService.getRole().subscribe((role) => (this.userRole = role.name));
  }

  getMedicines() {
    this.userMessage = 'Loading...';
    this.medicineService.getAll().subscribe(
      (m) => {
        this.userMessage = '';
        this.medicines = m;
        this.displayedData = this.medicines;
      },
      (err: any) =>
        (this.userMessage = `Error: status: ${err.status}, message: ${err.statusText}`)
    );
  }

  addMedicine(medicine: Medicine) {
    this.medicineService.add(medicine).subscribe(() => {
      this.getMedicines();
      this.showAddModal = false;
    });
  }

  deleteMedicine(id: string | number) {
    this.medicineService.delete(id).subscribe((res: any) => {
      this.getMedicines();
    }, this.handleError);
  }

  editableItem: Medicine | null = null;
  openEditModal(medicine: Medicine) {
    this.modalAction = 'edit';
    this.showAddModal = true;
    this.editableItem = medicine;
  }

  editMedicine(medicine: Medicine) {
    this.medicineService.update(medicine).subscribe(() => {
      this.getMedicines();
      this.showAddModal = false;
    });
  }

  search(searchString: string) {
    let search = searchString.toLowerCase().trim();
    if (search.length === 0) {
      this.displayedData = this.medicines;
    } else {
      this.displayedData = this.medicines?.filter((c) =>
        c.description.toLowerCase().includes(search)
      );
    }
  }

  showAddModal: boolean = false;
  openAddModal() {
    this.modalAction = 'add';
    this.showAddModal = true;
  }

  canAdd() {
    return this.userRole === Roles.Manager || this.userRole === Roles.Admin;
  }

  canModify() {
    return this.userRole === Roles.Manager || this.userRole === Roles.Admin;
  }

  canDelete() {
    return this.userRole === Roles.Manager || this.userRole === Roles.Admin;
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.status !== 0 && errorResponse.error?.msg) {
      alert(errorResponse.error.msg);
    }
  }
}
