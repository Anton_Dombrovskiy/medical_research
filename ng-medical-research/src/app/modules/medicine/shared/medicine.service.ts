import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, tap } from 'rxjs';
import { UtilsService } from '../../../shared/services/utils.service';
import { Medicine, MedicineState } from './medicine';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MedicineService {
  constructor(private utilsService: UtilsService, private http: HttpClient) {}
  private headerOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };
  getAll(): Observable<Medicine[]> {
    return this.http
      .get<Medicine[]>(`${environment.apiURL}/medicine`, this.headerOptions)
      .pipe(
        tap((m) => {
          m.forEach((med) => (med.expireAt = new Date(med.expireAt)));
        })
      );
  }

  add(medicine: Medicine): Observable<Medicine> {
    return this.http.post<Medicine>(
      `${environment.apiURL}/medicine`,
      medicine,
      this.headerOptions
    );
  }

  update(medicine: Medicine): Observable<Medicine> {
    console.log(medicine);

    return this.http.put<Medicine>(
      `${environment.apiURL}/medicine/${medicine.id}`,
      medicine,
      this.headerOptions
    );
  }

  delete(id: string | number) {
    return this.http.delete(
      `${environment.apiURL}/medicine/${id}`,
      this.headerOptions
    );
  }
}
