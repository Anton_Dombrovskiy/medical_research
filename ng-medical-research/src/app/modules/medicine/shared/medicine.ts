import { IEntity } from '../../../shared/interfaces/entity';

export interface Medicine extends IEntity {
  id: number;
  description: string;
  type: string;
  dosageForm: string;
  container: string;
  state: MedicineState;
  expireAt: Date;
}

export interface MedicineName {
  id: number;
  description: string;
}

export enum MedicineState {
  Ok,
  Damaged,
  Lost,
}
