import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';
import { MedicineGuard } from './medicine.guard';
import { MedicinePageComponent } from './pages/medicine-page/medicine-page.component';

const medicineRoutes = RoutesConfig.routesNames.medicine;

const medicinesRoutes: Routes = [
  {
    path: medicineRoutes.medicines,
    component: MedicinePageComponent,
    canActivate: [MedicineGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(medicinesRoutes)],
  exports: [RouterModule],
  providers: [MedicineGuard],
})
export class MedicineRoutingModule {}
