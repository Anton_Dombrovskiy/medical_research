export interface Supply {
  id: number;
  clinicName: string;
  medicineName: string;
  amount: number;
}

export interface SupplyPost {
  id: number;
  medicineId: number;
  clinicId: number;
  amount: number;
}

export interface ClinicName {
  id: number;
  name: string;
}

export interface MedicineName {
  id: number;
  description: string;
}
