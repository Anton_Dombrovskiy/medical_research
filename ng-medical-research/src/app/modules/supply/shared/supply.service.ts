import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, tap } from 'rxjs';
import { ClinicName, Supply, SupplyPost } from './supply';
import { environment } from '../../../../environments/environment';
import { MedicineName } from '../../medicine/shared/medicine';

@Injectable({
  providedIn: 'root',
})
export class SupplyService {
  private headerOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Content-Security-Policy': 'unsafe-inline',
    }),
  };
  constructor(private http: HttpClient) {}

  getAll(): Observable<Supply[]> {
    return this.http.get<Supply[]>(
      `${environment.apiURL}/supply`,
      this.headerOptions
    );
  }

  delete(id: number) {
    return this.http.delete(
      `${environment.apiURL}/supply/${id}`,
      this.headerOptions
    );
  }

  add(supply: SupplyPost): Observable<Supply> {
    return this.http.post<Supply>(
      `${environment.apiURL}/supply`,
      supply,
      this.headerOptions
    );
  }

  supply() {
    return this.http.post(
      `${environment.apiURL}/supply/SupplyMedicine`,
      {},
      this.headerOptions
    );
  }

  getClinicNames(): Observable<ClinicName[]> {
    return this.http.get<ClinicName[]>(
      `${environment.apiURL}/supply/GetAllClinicNames`
    );
  }

  getMedicineNames(): Observable<MedicineName[]> {
    return this.http.get<MedicineName[]>(
      `${environment.apiURL}/supply/GetAllMedicineNames`
    );
  }
}
