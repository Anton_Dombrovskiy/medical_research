import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MedicineName } from '../../../../modules/medicine/shared/medicine';
import { ClinicName, Supply, SupplyPost } from '../../shared/supply';
import { SupplyService } from '../../shared/supply.service';

@Component({
  selector: 'app-supply-page',
  templateUrl: './supply-page.component.html',
  styleUrls: ['./supply-page.component.css'],
})
export class SupplyPageComponent implements OnInit {
  tableHeaders = ['Clinic', 'Medicine', 'Amount'];
  supplies?: Supply[];
  clinicNames?: ClinicName[];
  medicineNames?: MedicineName[];
  clinicId: number | null = null;
  medicineId: number | null = null;
  amount?: number;
  constructor(private supplyService: SupplyService) {}

  userMessage: string = 'Loading...';
  getSupplies() {
    this.userMessage = 'Loading...';
    this.supplyService.getAll().subscribe(
      (s) => {
        this.userMessage = '';
        this.supplies = s;
      },
      (err: any) =>
        (this.userMessage = `Error: status: ${err.status}, message: ${err.statusText}`)
    );
  }

  getMedicineNames() {
    this.supplyService
      .getMedicineNames()
      .subscribe((mNames) => (this.medicineNames = mNames));
  }

  getClinicNames() {
    this.supplyService
      .getClinicNames()
      .subscribe((cNames) => (this.clinicNames = cNames));
  }

  deleteSupply(id: number | string) {
    this.supplyService.delete(+id).subscribe(() => this.getSupplies());
  }

  addMedicine(supplyForm: NgForm) {
    if (supplyForm.valid) {
      this.supplyService
        .add({
          id: 1,
          medicineId: this.medicineId!,
          clinicId: this.clinicId!,
          amount: this.amount!,
        } as SupplyPost)
        .subscribe(() => {
          this.getSupplies();
          supplyForm.reset();
        });
    }
  }

  supplyMedicine() {
    if (confirm('Confirm acntion')) {
      this.supplyService.supply().subscribe(() => this.getSupplies());
    }
  }

  ngOnInit(): void {
    this.getSupplies();
    this.getClinicNames();
    this.getMedicineNames();
  }
}
