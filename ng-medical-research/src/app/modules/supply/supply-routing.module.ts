import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesConfig } from '../../configs/routes.config';

import { SupplyPageComponent } from './pages/supply-page/supply-page.component';
import { SupplyGuard } from './supply.guard';

const supplyRoutes = RoutesConfig.routesNames.supply;

const suppliesRoutes: Routes = [
  {
    path: supplyRoutes.supplies,
    component: SupplyPageComponent,
    canActivate: [SupplyGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(suppliesRoutes)],
  exports: [RouterModule],
  providers: [SupplyGuard],
})
export class SupplyRoutingModule {}
