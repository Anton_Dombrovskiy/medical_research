import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { SupplyPageComponent } from './pages/supply-page/supply-page.component';
import { SupplyRoutingModule } from './supply-routing.module';

@NgModule({
  declarations: [SupplyPageComponent],
  imports: [CommonModule, SharedModule, SupplyRoutingModule, FormsModule],
})
export class SupplyModule {}
