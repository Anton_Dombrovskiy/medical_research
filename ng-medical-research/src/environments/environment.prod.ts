export const environment = {
  production: true,
  apiURL: '/api',
  projectImgURL: '/research-project-backgrounds',
  userAvatarURL: '/user-avatars',
};
